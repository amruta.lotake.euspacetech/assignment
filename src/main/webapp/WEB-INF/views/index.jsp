<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="icon" href="${pageContext.request.contextPath}/static/images/favicon.png" type="image/png" sizes="16x16">
<title>EU SPACE</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/app.css">

</head>

<body>
	<h1>EUSPACE Springboot Rest Framework</h1>
	<script src="${pageContext.request.contextPath}/static/js/app.js"></script>
</body>

</html>