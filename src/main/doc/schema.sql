DROP DATABASE crud;

CREATE DATABASE crud
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'en_IN'
       LC_CTYPE = 'en_IN'
       CONNECTION LIMIT = -1;

CREATE TABLE system_configs (
	config_key character varying(100) NOT NULL,
	config_value character varying(3000) NOT NULL,
	environment character varying(20) NOT NULL,
	PRIMARY KEY (config_key, environment)
)
WITH (
	OIDS=FALSE
) ;

CREATE TABLE app_versions
(
	app_version_id character(32) NOT NULL,
    version_no character varying(20) NOT NULL,
    app_type character varying(100) NOT NULL,
    device_type character varying(100) NOT NULL,
    app_link character varying(1000) NOT NULL,
    release_date bigint NOT NULL,
    is_mandatory boolean NOT NULL,
    remove_support_from bigint,
    created_at bigint,
    created_by character(32),
    updated_at bigint,
    updated_by character(32),
    deleted boolean,
    CONSTRAINT app_version_id_pk PRIMARY KEY (app_version_id)
)
WITH (
    OIDS = FALSE
);

CREATE TABLE app_version_release_notes
(
	app_version_release_note_id character(32) NOT NULL,
	app_version_id character varying(32) NOT NULL,
    language_code character varying(100) NOT NULL,
    release_notes text NOT NULL,
    created_at bigint,
    created_by character(32),
    updated_at bigint,
    updated_by character(32),
    deleted boolean,
    CONSTRAINT app_version_release_note_id_pk PRIMARY KEY (app_version_release_note_id),
    CONSTRAINT fk_app_version_release_notes_app_versions_app_version_id FOREIGN KEY (app_version_id)
      REFERENCES app_versions (app_version_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);


CREATE TABLE users (
  id SERIAL NOT NULL,
  user_id character(32) NOT NULL,
  username character varying(300) ,
  password character varying(300) NOT NULL,
  email character varying(300) NOT NULL,
  first_name character varying(30) NOT NULL,
  last_name character varying(30) NOT NULL,
  date_of_birth bigint NOT NULL,
  phone_number character varying(15) NOT NULL,
  
  profile_image character varying(70) ,
  credentials_expired boolean default false,
  account_enabled boolean default false,
  account_expired boolean default false,
  account_locked boolean default false,
  created_at bigint,
  created_by character(32),
  updated_at bigint,
  updated_by character(32),
  is_active boolean default true,
  deleted boolean default false,
  CONSTRAINT users_pkey PRIMARY KEY (user_id),
  CONSTRAINT unique_username UNIQUE (username)
)
WITH (
  OIDS=FALSE
);


CREATE TABLE roles (
 
  role_id int NOT NULL,
  role_name character varying(25) NOT NULL,
  role_code character varying(100) NOT NULL,
  created_at bigint,
  created_by character(32),
  updated_at bigint,
  updated_by character(32),
  is_active boolean default true,
  deleted boolean default false,
  CONSTRAINT roles_pkey PRIMARY KEY (role_id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE user_roles (
  user_role_id character(32) NOT NULL,
  role_id int NOT NULL,
  user_id character(32) NOT NULL,
  created_at bigint,
  created_by character(32),
  updated_at bigint,
  updated_by character(32),
  is_active boolean default true,
  deleted boolean default false,
  CONSTRAINT user_roles_pkey PRIMARY KEY (user_role_id),
  CONSTRAINT fk_user_roles_roles_role_id FOREIGN KEY (role_id)
      REFERENCES roles (role_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_user_roles_users_user_id FOREIGN KEY (user_id)
      REFERENCES users (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

CREATE TABLE api_sessions (
  api_session_id character(32),
  user_id character(32) NOT NULL,
  device_uid character varying(250) ,
  expires_at bigint NOT NULL,
  created_at bigint,
  created_by character(32),
  updated_at bigint,
  updated_by character(32),
  is_active boolean default true,
  deleted boolean default false,
  CONSTRAINT api_session_id_pk PRIMARY KEY (api_session_id),
  CONSTRAINT fk_api_sessions_users_user_id FOREIGN KEY (user_id)
      REFERENCES users (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

CREATE TABLE user_devices
(
  user_device_id character(32) NOT NULL,
  user_id character(32) NOT NULL,
  device_uid character varying(250) NOT NULL,
  device_token character varying(250),
  device_name character varying(250),
  device_type character varying(250),
  created_at bigint,
  created_by character(32),
  updated_at bigint,
  updated_by character(32),
  is_active boolean default true,
  deleted boolean default false,
  CONSTRAINT user_device_id_pk PRIMARY KEY (user_device_id),
  CONSTRAINT user_devices_user_id_fk FOREIGN KEY (user_id)
      REFERENCES users (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE INDEX user_device_user_id_idx ON user_devices (user_id);
CREATE INDEX user_device_device_uidx ON user_devices (device_uid);
CREATE INDEX user_device_device_tokenx ON user_devices (device_token);


CREATE TABLE role_accesses
(
  role_access_id character(32) NOT NULL,
  role_id int NOT NULL,
  access_name character varying(250) NOT NULL,
  access_group character varying(250) NOT NULL,
  created_at bigint,
  created_by character(32),
  updated_at bigint,
  updated_by character(32),
  is_active boolean default true,
  deleted boolean default false,
  CONSTRAINT role_access_id_pk PRIMARY KEY (role_access_id),
  CONSTRAINT role_accesses_roles_role_id_fk FOREIGN KEY (role_id)
      REFERENCES roles (role_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);
CREATE INDEX role_accesses_access_name_idx ON role_accesses (access_name);
CREATE INDEX role_accesses_access_group_idx ON role_accesses (access_group);
CREATE INDEX role_accesses_role_id_idx ON role_accesses (role_id);






CREATE TABLE user_images (
  user_images_id character(32) NOT NULL,
  user_id character(32) NOT NULL,
  image character varying(50) NOT NULL,
  default_image boolean default false,
  created_at bigint,
  created_by character(32),
  updated_at bigint,
  updated_by character(32),
  is_active boolean default true,
  deleted boolean default false,
  CONSTRAINT items_images_pkey PRIMARY KEY (user_images_id),
  CONSTRAINT fk_items_images_items_items_id FOREIGN KEY (user_id)
      REFERENCES users (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

CREATE TABLE public.files
(
  file_id character(32) NOT NULL,
  file_url character varying(100),
  bucket_id character(32),
  file_size double precision,
  file_name character varying(100),
  created_by character varying(32),
  created_at bigint,
  updated_by character varying(32),
  updated_at bigint,
  deleted boolean DEFAULT false,
  is_active boolean DEFAULT true,
  CONSTRAINT files_pkey PRIMARY KEY (file_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.files
  OWNER TO postgres;




