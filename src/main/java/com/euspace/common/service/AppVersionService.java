package com.euspace.common.service;

import java.util.List;

import com.euspace.common.model.AppVersion;
import com.euspace.common.model.AppVersionReleaseNote;

public interface AppVersionService {
	AppVersion retriveVersionByVersion(String appType, String deviceType, String version);
	AppVersion retriveLatestVersion(String appType, String deviceType);
	boolean checkMandatoryReleaseAvailable(String appType, String deviceType, long releaseDate);
	List<AppVersionReleaseNote> retriveReleaseNotesForVersion(String appVersionId);

}
