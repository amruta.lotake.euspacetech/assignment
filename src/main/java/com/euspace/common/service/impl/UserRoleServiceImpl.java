package com.euspace.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.euspace.common.dao.UserRoleDao;
import com.euspace.common.model.UserRoleModel;
import com.euspace.common.service.UserRoleService;

@Service
public class UserRoleServiceImpl implements UserRoleService {

	@Autowired
	private UserRoleDao userRoleDao;

	public int grantRoles(final List<UserRoleModel> roles) {
		return userRoleDao.grantRoles(roles);
	}

}