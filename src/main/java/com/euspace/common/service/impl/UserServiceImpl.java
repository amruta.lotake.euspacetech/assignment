package com.euspace.common.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.euspace.common.dao.ApiSessionDao;
import com.euspace.common.dao.UserDao;
import com.euspace.common.dao.UserDeviceDao;
import com.euspace.common.dao.UserRoleDao;
import com.euspace.common.dto.UserAddDto;
import com.euspace.common.dto.UserDeviceDto;
import com.euspace.common.dto.UserDto;
import com.euspace.common.model.ApiSession;
import com.euspace.common.model.Role;
import com.euspace.common.model.User;
import com.euspace.common.model.UserDevice;
import com.euspace.common.model.UserRoleModel;
import com.euspace.common.security.UserRole;
import com.euspace.common.service.ApiSessionService;
import com.euspace.common.service.UserService;
import com.euspace.common.utils.Constants;
import com.euspace.common.utils.DateUtils;
import com.euspace.common.utils.UUID;


@Service
public class UserServiceImpl implements UserService {
	private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);
	private UserDao userDao;
	private UserRoleDao userRoleDao;
	private UserDeviceDao userDeviceDao;
	private ApiSessionDao apiSessionDao;
	@Autowired
	private ApiSessionService sessionService;
	@Autowired(required = true)
	public UserServiceImpl(UserDao userDao, UserRoleDao userRoleDao, UserDeviceDao userDeviceDao, ApiSessionDao apiSessionDao) {
		this.userDao = userDao;
		this.userRoleDao = userRoleDao;
		this.userDeviceDao = userDeviceDao;
		this.apiSessionDao = apiSessionDao;
	}

	public Optional<User> findByUsername(String username) {
		return userDao.findByUsername(username);
	}

	@Transactional
	public User getUserWithItsAccesses(String userId) {
		User user = userDao.findById(userId);
		return getUserWithAccesses(user);
	}

	@Transactional
	public User getUserWithAccesses(User user) {
		Set<String> roleList = userRoleDao.retriveUserRoles(user.getUserId());
		Set<UserRole> roles = new HashSet<>();
		for (String i : roleList) {
			if(i.equals(Constants.ROLE_USER_ID)) {
				UserRole role = UserRole.valueOf(1);
				roles.add(role);
			}else if(i.equals(Constants.ROLE_ADMIN_ID)) {
				UserRole role = UserRole.valueOf(2);
				roles.add(role);
			}else if(i.equals(Constants.ROLE_WORKER_ID)) {
				UserRole role = UserRole.valueOf(3);
				roles.add(role);
			}
		}
		user.setRoles(roles);
		return user;
	}

	public User findById(String userId) {
		return userDao.findById(userId);
	}

	public boolean isUserExist(String username) {
		return userDao.userExists(username);
	}

	public boolean isUserEmailExist(String email) {
		return userDao.emailExists(email);
	}
	

	@Transactional
	public Map<String, Object> signupUser(UserAddDto addUser, List<Role> roles) {
		String userId = UUID.getId();
		long now = DateUtils.now();
		long expireAt = now + Constants.TEN_DAYS;
		User user = prepareUserModel(addUser);

		user.setUserId(userId);
		user.setPassword(new BCryptPasswordEncoder().encode(addUser.getPassword()));
		user.setActive(true);
		user.setCreatedBy(userId);
		user.setUpdatedBy(userId);
		user.setCreatedAt(now);
		user.setUpdatedAt(now);

		List<UserRoleModel> userRoles = new ArrayList<>();
		for (Role role : roles) {
			UserRoleModel userRole = new UserRoleModel();
			userRole.setUserRoleId(UUID.getId());
			userRole.setRoleId(role.getRoleId());
			userRole.setUserId(userId);
			userRole.setActive(true);
			userRole.setCreatedBy(userId);
			userRole.setUpdatedBy(userId);
			userRole.setCreatedAt(now);
			userRole.setUpdatedAt(now);
			userRoles.add(userRole);
		}

		userDao.addUser(user);
		userRoleDao.grantRoles(userRoles);
		ApiSession session = null;
		UserDevice device = getUserDevice(addUser.getDeviceInfo());
		if (device.getDeviceUid() != null) {
			device.setUserDeviceId(UUID.getId());
			device.setUserId(userId);
			device.setActive(true);
			device.setCreatedBy(userId);
			device.setUpdatedBy(userId);
			device.setCreatedAt(now);
			device.setUpdatedAt(now);

			userDeviceDao.addUserDevice(device);

			session = new ApiSession(userId);
			session.setDeviceUUID(device.getDeviceUid());
			session.setExpiresAt(expireAt);
			session.setActive(true);
			session.setCreatedBy(userId);
			session.setUpdatedBy(userId);
			session.setCreatedAt(now);
			session.setUpdatedAt(now);
		} else {

			session = new ApiSession(userId);
			session.setExpiresAt(expireAt);
			session.setActive(true);
			session.setCreatedBy(user.getUserId());
			session.setUpdatedBy(user.getUserId());
			session.setCreatedAt(now);
			session.setUpdatedAt(now);

		} 

		apiSessionDao.addSession(session);

		Optional<List<Role>> rolesList = userDao.getUserRoles(userId);

		Map<String, Object> result = new HashMap<>();
		result.put("profile", userDao.userDetailsByEmail("email",addUser.getEmail()));
		result.put("sessionKey", session.getApiSessionId());
		Optional<UserDevice> userDeviceDetails = getUserDeviceDetails(userId);
		if (userDeviceDetails.isPresent()) {
			result.put("deviceDetails", userDeviceDetails.get());
		}
		if (rolesList.isPresent()) {
			result.put("role", rolesList.get());
		}

		return result;
	}
	private User prepareUserModel(UserAddDto userAddDto) {

		User user = new User();
		user.setFirstName(userAddDto.getFirstName());
		user.setLastName(userAddDto.getLastName());
		Long dateOfBirth = DateUtils.getTimestampFromDateString(userAddDto.getDateOfBirth());
		user.setDateOfBirth(dateOfBirth);
		user.setProfileImage(userAddDto.getProfileImage());
		user.setPhoneNumber(userAddDto.getPhoneNumber());

		user.setEmail(userAddDto.getEmail());

		return user;

	}
	private static UserDevice getUserDevice(UserDeviceDto deviceInfo) {
		UserDevice device = new UserDevice();

		if (deviceInfo != null) {
			device.setDeviceUid(deviceInfo.getDeviceUid());
			device.setDeviceToken(deviceInfo.getDeviceToken());
			device.setDeviceName(deviceInfo.getDeviceName());
			device.setDeviceType(deviceInfo.getDeviceType());
		}

		return device;
	}
	public Optional<UserDevice> getUserDeviceDetails(String userId) {
		return userDeviceDao.getUserDeviceDetails(userId);
	}
	public boolean isPhoneNumberExistsForOtherUsers(String phoneNumber, String userId) {
		boolean status = false;
		try {
			status = userDao.isPhoneNumberExistsForOtherUsers(phoneNumber, userId);
			return status;
		} catch (Exception e) {
			LOG.error("Exception in phoneNumberExistsForOtherUsers", e);
			return false;
		}
	}

	@Override
	public boolean isEmailExistsForOtherUsers(String email, String userId) {
		boolean status = false;
		try {
			status = userDao.isEmailExistsForOtherUsers(email, userId);
			return status;
		} catch (Exception e) {
			LOG.error("Exception in emailExistsForOtherUsers", e);
			return false;
		}
	}

	@Override
	public Optional<UserDto> getUserCompleteDetailsByUserId(String userId) {

		Optional<UserDto> userDetails = userDao.getUserCompleteDetailsById(userId);

		if (userDetails.isPresent()) {

			return userDetails;
		}
		return Optional.empty();
	}
	@Override
	public Map<String, Object> validateUserSignIn(String field, String value, UserAddDto user) {
		Map<String, Object> result = new HashMap<>();

		String password = user.getPassword();
		boolean passwordStatus = false;
		Optional<UserDto> userAddDto = Optional.empty();
		long now = DateUtils.now();
		long expireAt = now + Constants.TEN_DAYS;
		ApiSession session = new ApiSession();
		try {
			Optional<String> userPassword = userDao.getUserPasswordByField(field, value.toLowerCase());

			if (userPassword.isPresent()) {

				BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
				passwordStatus = passwordEncoder.matches(password, userPassword.get());

				if (passwordStatus) {

					passwordStatus = true;
					userAddDto = userDao.userDetailsByEmail(field, value);

					if (userAddDto.isPresent()) {
						String userId = userAddDto.get().getUserId();

						UserDevice userDevice = getUserDevice(user.getDeviceInfo());

						if (userDevice.getDeviceUid() != null) {

							userDevice.setUserDeviceId(UUID.getId());
							userDevice.setUserId(userId);
							userDevice.setActive(true);
							userDevice.setCreatedBy(userId);
							userDevice.setUpdatedBy(userId);
							userDevice.setCreatedAt(now);
							userDevice.setUpdatedAt(now);

							addUserDevice(userDevice);

							session = new ApiSession(userId);
							session.setDeviceUUID(userDevice.getDeviceUid());
							session.setExpiresAt(expireAt);
							session.setActive(true);
							session.setCreatedBy(userId);
							session.setUpdatedBy(userId);
							session.setCreatedAt(now);
							session.setUpdatedAt(now);

						} else {

							session = new ApiSession(userId);
							session.setExpiresAt(expireAt);
							session.setActive(true);
							session.setCreatedBy(user.getUserId());
							session.setUpdatedBy(user.getUserId());
							session.setCreatedAt(now);
							session.setUpdatedAt(now);

						}

						sessionService.addSession(session);

						String roleName = "";
						Optional<List<Role>> rolesList = getUserRoleListByUserId(userId);
						for (Role obj : rolesList.get()) {
							roleName = obj.getRoleName();
						}
						if (Constants.ROLE_WORKER_STR.equalsIgnoreCase(roleName)) {
			 				if (user.getDeviceInfo() == null) {
								return result;
							}
							Optional<UserDevice> userDeviceDetails = getUserDeviceDetails(userId);
							if (!userDeviceDetails.isPresent()) {
								return result;
							}
							userAddDto.get().setPassword("");
							if (rolesList.isPresent()) {
								result.put("roleList", rolesList.get());
							}
							result.put("sessionKey", session.getApiSessionId());
							result.put("profile", userAddDto.get());

							result.put("deviceDetails", userDeviceDetails.get());

						} else if (Constants.ROLE_ADMIN_STR.equalsIgnoreCase(roleName)) {

							userAddDto.get().setPassword("");
							result.put("profile", userAddDto.get());
							result.put("sessionKey", session.getApiSessionId());
							if (user.getDeviceInfo() != null) {
								Optional<UserDevice> userDeviceDetails = getUserDeviceDetails(userId);
								if (userDeviceDetails.isPresent()) {
									result.put("deviceDetails", userDeviceDetails.get());
								}
							}
							if (rolesList.isPresent()) {
								result.put("roleList", rolesList.get());
							}
						} else if (Constants.ROLE_USER_STR.equalsIgnoreCase(roleName)) {

							userAddDto.get().setPassword("");
							result.put("profile", userAddDto.get());
							result.put("sessionKey", session.getApiSessionId());
							if (user.getDeviceInfo() != null) {
								Optional<UserDevice> userDeviceDetails = getUserDeviceDetails(userId);
								if (userDeviceDetails.isPresent()) {
									result.put("deviceDetails", userDeviceDetails.get());
								}
							}
							if (rolesList.isPresent()) {
								result.put("roleList", rolesList.get());
							}
						}

					}
				}
				return result;

			}

		} catch (Exception e) {

			LOG.error("Exception in validate User", e);

		}

		return result;

	}

	public int addUserDevice(UserDevice userDevice) {

		return userDeviceDao.addUserDevice(userDevice);
	}
	
	@Override
	public Optional<List<Role>> getUserRoleListByUserId(String userId) {
		Optional<List<Role>> rolesList = userDao.getUserRoles(userId);

		if (rolesList.isPresent()) {

			return rolesList;
		} else {
			return Optional.empty();
		}
	}
	
	@Override
	public int deleteUser(int userId) {
		int status = 0;
		try {
		
			status = userDao.deleteUser(userId);

		} catch (Exception e) {
			LOG.error("Exception in deleteUsersDetails", e);
		}
		return status;
	}
	
	@Override
	public int updateUser(UserDto userDto) {
		int status = 0;
		try {
		
			status = userDao.updateUser(userDto);

		} catch (Exception e) {
			LOG.error("Exception in deleteUsersDetails", e);
		}
		return status;
	}
	
	@Override
	public Optional<List<UserDto>> getListOfUser() {
		try {
			return userDao.getAllUserList();

		} catch (Exception e) {
			((Logger) LOG).error("Exception in getCategoryDetailsById", e);
			return Optional.empty();
		}
	}

}