package com.euspace.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.euspace.common.dao.SystemConfigDao;
import com.euspace.common.model.SystemConfig;
import com.euspace.common.service.SystemConfigService;

@Service
public class SystemConfigServiceImpl implements SystemConfigService{

	@Autowired
	private SystemConfigDao configDao;

	public List<SystemConfig> fetchAllEnvConfig(String env) {
		return this.configDao.fetchAllEnvConfig(env);
	}

}
