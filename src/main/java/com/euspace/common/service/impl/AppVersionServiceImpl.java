package com.euspace.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.euspace.common.dao.AppVersionDao;
import com.euspace.common.model.AppVersion;
import com.euspace.common.model.AppVersionReleaseNote;
import com.euspace.common.service.AppVersionService;

@Service
public class AppVersionServiceImpl implements AppVersionService{

	@Autowired
	private AppVersionDao appVersionDao;

	@Transactional
	public AppVersion retriveVersionByVersion(String appType, String deviceType, String version) {
		return appVersionDao.retriveVersionByVersion(appType, deviceType, version);
	}

	@Transactional
	public AppVersion retriveLatestVersion(String appType, String deviceType) {
		return appVersionDao.retriveLatestVersion(appType, deviceType);
	}

	@Transactional
	public boolean checkMandatoryReleaseAvailable(String appType, String deviceType, long releaseDate) {
		return appVersionDao.checkMandatoryReleaseAvailable(appType, deviceType, releaseDate);
	}

	@Transactional
	public List<AppVersionReleaseNote> retriveReleaseNotesForVersion(String appVersionId) {
		return appVersionDao.retriveReleaseNotesForVersion(appVersionId);
	}

}
