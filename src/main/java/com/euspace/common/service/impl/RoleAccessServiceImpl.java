package com.euspace.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.euspace.common.dao.RoleAccessDao;
import com.euspace.common.model.RoleAccess;
import com.euspace.common.service.RoleAccessService;

@Service
public class RoleAccessServiceImpl implements RoleAccessService {
	
	@Autowired
	private RoleAccessDao roleAccessDao;

	@Autowired(required = true)
	public RoleAccessServiceImpl(RoleAccessDao roleAccessDao) {
		this.roleAccessDao = roleAccessDao;
	}

	public List<RoleAccess> fetchAllAccessForRole(int roleId) {
		return this.roleAccessDao.fetchAllAccessForRole(roleId);
	}

	public boolean doesUserHasAccess(String accessName, String userId) {
		// return this.roleAccessDao.doesUserHasAccess(accessName, userId);
		// TODO need to provide role wise access
		return true;
	}

}