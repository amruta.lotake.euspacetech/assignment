package com.euspace.common.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.euspace.common.dao.ApiSessionDao;
import com.euspace.common.model.ApiSession;
import com.euspace.common.service.ApiSessionService;
import com.euspace.common.utils.DateUtils;

@Service
public class ApiSessionServiceImpl implements ApiSessionService {

	@Autowired
	private ApiSessionDao sessionMapper;

	public int addSession(ApiSession session) {
		long now = DateUtils.now();
		session.setUpdatedAt(now);
		session.setCreatedAt(now);
		session.setUpdatedBy(session.getUserId());
		session.setCreatedBy(session.getUserId());
		session.setActive(true);
		return sessionMapper.addSession(session);
	}

	public ApiSession fetchSession(String apiSessionId) {
		return sessionMapper.fetchSession(apiSessionId);
	}

	public int deleteSession(String sessionKey) {
		return sessionMapper.deleteSession(sessionKey);
	}

}