package com.euspace.common.service;

import java.util.List;

import com.euspace.common.model.RoleAccess;

public interface RoleAccessService {

	List<RoleAccess> fetchAllAccessForRole(int roleId);
	boolean doesUserHasAccess(String accessName, String userId);
}
