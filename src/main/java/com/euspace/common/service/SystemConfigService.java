package com.euspace.common.service;

import java.util.List;

import com.euspace.common.model.SystemConfig;

public interface SystemConfigService {
	
	List<SystemConfig> fetchAllEnvConfig(String env);

}
