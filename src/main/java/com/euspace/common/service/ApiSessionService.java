package com.euspace.common.service;

import com.euspace.common.model.ApiSession;

public interface ApiSessionService {

	int addSession(ApiSession session);
	ApiSession fetchSession(String apiSessionId);
	int deleteSession(String sessionKey);
	
}
