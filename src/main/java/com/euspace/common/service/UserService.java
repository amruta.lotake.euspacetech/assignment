package com.euspace.common.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.euspace.common.dto.UserAddDto;
import com.euspace.common.dto.UserDto;
import com.euspace.common.model.Role;
import com.euspace.common.model.User;

public interface UserService {
	
	Optional<User> findByUsername(String username);
	
	User getUserWithItsAccesses(String userId);
	
	User getUserWithAccesses(User user);
	
	User findById(String userId);
	
	boolean isPhoneNumberExistsForOtherUsers(String phoneNumber, String userId);
	
	boolean isEmailExistsForOtherUsers(String email, String userId);
	
	boolean isUserExist(String username);
	
	boolean isUserEmailExist(String email);
	
	Map<String, Object> signupUser(UserAddDto addUser, List<Role> roles);
	
	Optional<UserDto> getUserCompleteDetailsByUserId(String userId);

	Optional<List<Role>> getUserRoleListByUserId(String userId);

	Map<String, Object> validateUserSignIn(String field, String value, UserAddDto user);

	int deleteUser(int userId);

	int updateUser(UserDto userDto);

	Optional<List<UserDto>> getListOfUser();
}
