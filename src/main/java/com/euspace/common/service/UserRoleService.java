package com.euspace.common.service;

import java.util.List;

import com.euspace.common.model.UserRoleModel;

public interface UserRoleService {
	
	int grantRoles(final List<UserRoleModel> roles);

}
