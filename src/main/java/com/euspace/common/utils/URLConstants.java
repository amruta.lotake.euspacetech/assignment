package com.euspace.common.utils;


public interface URLConstants {

	public static final String METHOD_GET = "get";
	public static final String METHOD_POST = "post";
	public static final String METHOD_PUT = "put";
	public static final String METHOD_DELETE = "delete";

	public static final String REQUEST = "request";
	public static final String RESTTEMPLATE = "restTemplate";

	public static final String CONTENT_TYPE = "Content-Type";
	public static final String ACCEPT_LANGUAGE = "Accept-Language";
	public static final String REQUEST_HEADER_X_APP_TYPE = "x-app-type";
	public static final String AUTHORIZATION = "Authorization";
	public static final String BEARER = "Bearer ";

	public static final String BASE_URL = "http://localhost:8099/fruitapp/api";
	
	//public static final String BASE_URL = "http://localhost:8081/matrimonialproject/api/";
	
	//public static final String BASE_URL = "http://matrimonial-env-1.ppfpfutkpj.us-east-2.elasticbeanstalk.com/api/";
	
	public static final String SIGN_IN = "/systemuser/signin";
	
	public static final String USER_SIGN_IN="/user/signin";
	public static final String USER_PROFILE_COMPLETENESS="/secure/user/profile_completeness/";
	public static final String USER_PROFILE_PICTURE="/secure/user/profile-picture/";
	public static final String GET_USER_DETAILS_BY_ID = "/secure/user/";
	public static final String GET_USER_PARTNER_PREFERENCES_DETAILS_BY_ID = "/secure/user-partner-preferences/";
	
	
	public static final String FILE_UPLOAD="/secure/fileupload";
	public static final String TEMP_ID="12345678901234567890123456789012";
	
	public static final String FILE_DOWNLOAD=BASE_URL + "/downloadfile/";
	public static final String FILE_THUMBNAIL="thumbnail_";
	public static final String ADD_GALLERY_DETAILS="/secure/user/gallery-details";
	public static final String DELETE_IMAGE_DETAILS="/secure/user/gallery-details/";
	

	
}
