package com.euspace.common.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtils {

	private static final Logger LOG = LoggerFactory.getLogger(DateUtils.class);

	public static final String DATE_FORMAT_STR = "dd/MM/yyyy";
	public static final String DATE_FORMAT_STR_NEW = "dd-MM-yyyy";
	public static final String UTC_TIMEZONE = "UTC";

	private DateUtils() {
		// No Use
	}

	public static long now() {
		return new Date().getTime();
	}
	public static String getFormattedDateNew(long timeInMilliSeconds) {
		Calendar calendar = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat(DATE_FORMAT_STR_NEW);
		formatter.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		calendar.setTimeInMillis(timeInMilliSeconds);
		return formatter.format(calendar.getTime());
	}
	
	public static String getFormattedDate(long timeInMilliSeconds) {
		Calendar calendar = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat(DATE_FORMAT_STR);
		formatter.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		calendar.setTimeInMillis(timeInMilliSeconds);
		return formatter.format(calendar.getTime());
		
		
//		  DateFormat utcFormat = new SimpleDateFormat(patternString);
//		    utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
//		    DateFormat indianFormat = new SimpleDateFormat(patternString);
//		    indianFormat .setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
//		    Date timestamp = utcFormat.parse(inputString);
//		    String output = indianFormat.format(timestamp);
	}
	
	
	

	public static String getFormattedDate(long timeInMilliSeconds, String dateFormat) {
		Calendar calendar = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat(dateFormat);
		formatter.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		calendar.setTimeInMillis(timeInMilliSeconds);
		return formatter.format(calendar.getTime());
	}

	public static long getTimestampFromDateString(String dateStr) {
		SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT_STR);
		dateFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		Date date = null;
		try {
			date = dateFormatter.parse(dateStr);
			return date.getTime();
		} catch (ParseException e) {
			LOG.error("Error while parsing string date. Please check date format.", e);
			return 0;
		}
	}

	public static String getStringTime(long aDate, String pattern) {
		Date date = new Date(aDate);
		SimpleDateFormat df2 = new SimpleDateFormat(pattern);
		df2.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		String dateText = df2.format(date);
		dateText = dateText.replace("24", "00");
		return dateText;
	}

	public static long getEndOfDay(long date, String timezone) {

		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		TimeZone tz = TimeZone.getTimeZone(timezone);
		calendar.setTimeZone(tz);
		calendar.setTimeInMillis(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTimeInMillis();
	}

	public static long getStartOfDay(long date, String timezone) {
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		TimeZone tz = TimeZone.getTimeZone(timezone);
		calendar.setTimeZone(tz);
		calendar.setTimeInMillis(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTimeInMillis();
	}

	public static long addDaysInDateTime(int days, long date) {
		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_STR);
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.setTimeInMillis(date);
		calendar.add(Calendar.DATE, days);
		return calendar.getTimeInMillis();
	}

	public static long getTimeInSeconds(String time) {

		String[] timeInSeconds = time.split(":");

		int hour = Integer.parseInt(timeInSeconds[0]);
		int minute = Integer.parseInt(timeInSeconds[1]);
	//	int second=Integer.parseInt(timeInSeconds[2]);

		int temp;
		temp = (60 * minute) + (3600 * hour);

		System.out.println("secondsss" + temp);

		return temp;

	}
	
	///Saroj
	public static long getStartOfDay(long date) {

		Calendar calendar = Calendar.getInstance();
		
		calendar.clear();
		TimeZone tz = TimeZone.getTimeZone("India Standard Time");
		calendar.setTimeZone(tz);
		calendar.setTimeInMillis(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		return calendar.getTimeInMillis();
	}

	public static long getEndOfDay(long date) {

		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		TimeZone tz = TimeZone.getTimeZone("India Standard Time");
		calendar.setTimeZone(tz);
		calendar.setTimeInMillis(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTimeInMillis();
	}
	
	public static long getTommarrowOfDay(long date) {

		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		TimeZone tz = TimeZone.getTimeZone("India Standard Time");
		calendar.setTimeZone(tz);
		calendar.setTimeInMillis(date);
		calendar.set(Calendar.HOUR_OF_DAY, 24);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);
		calendar.set(Calendar.MILLISECOND,001);
		return calendar.getTimeInMillis();
	}
	
	
	
	

}