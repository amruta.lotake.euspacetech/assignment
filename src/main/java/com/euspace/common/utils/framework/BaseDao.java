package com.euspace.common.utils.framework;

import java.lang.reflect.Method;
import java.rmi.UnexpectedException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.euspace.common.exception.DbOperationException;



public class BaseDao {
	//private DataSource dataSource;
	protected Connection connection;

	private static final Logger LOGGER = LoggerFactory.getLogger(BaseDao.class);

	protected static final boolean STATUS_NOT_DELETED = false;
	protected static final boolean STATUS_DELETED = true;

	protected static final boolean STATUS_NOT_ACTIVATED = false;
	protected static final boolean STATUS_ACTIVATED = true;

	protected static final boolean STATUS_NOT_VERIFIED = false;
	protected static final boolean STATUS_VERIFIED = true;

	protected static final boolean STATUS_NOT_APPROVED = false;
	protected static final boolean STATUS_APPROVED = true;

	protected static final boolean STATUS_NOT_ACKNOWLEDGED = false;
	protected static final boolean STATUS_ACKNOWLEDGED = true;

	protected static final boolean STATUS_NOT_NOTIFY_TO_ACKNOWLEDGE = false;
	protected static final boolean STATUS_NOTIFY_TO_ACKNOWLEDGE = true;
	
	private String getField(String fieldNames) {
		String[] fields = fieldNames.split("_");
		StringBuilder fieldList = new StringBuilder();
		if (fields.length > 1) {
			for (int activeField = 0; activeField < fields.length; activeField++) {
				fieldList.append(Character.toUpperCase(fields[activeField].charAt(0))).append(fields[activeField].substring(1));
			}
		} else {
			fieldList.append(fields[0]);
		}
		return fieldList.toString().trim();
	}

	public int executeInsertUpdate(String sqlQuery, Object obj, Connection con) throws UnexpectedException {
		List<Object> param = new ArrayList<>();

		String sql = sqlQuery.trim().toLowerCase();
		if (sql.contains("\t")) {
			sql = sql.replaceAll("\t", " ");
		}
		String operation;
		String[] cols;
		if (sql.startsWith("update ")) {
			String[] colsTemp1 = sql.substring(sql.indexOf("set ") + 4, sql.indexOf("where ")).split(",");
			String[] colsTemp2 = sql.substring(sql.indexOf("where ") + 6, sql.length() - 1).split("and");
			cols = new String[colsTemp1.length + colsTemp2.length];
			for (int i = 0; i < colsTemp1.length; i++) {
				String col = colsTemp1[i];
				cols[i] = col.split("=")[0].trim();
			}
			for (int i = 0; i < colsTemp2.length; i++) {
				String col = colsTemp2[i];
				cols[colsTemp1.length + i] = col.split("=")[0].trim();
			}
			operation = "update";
		} else if (sql.startsWith("insert ")) {
			cols = sql.substring(sql.indexOf('(') + 1, sql.indexOf(')')).split(",");
			operation = "insert";
		} else {
			throw new UnexpectedException("Not an insert or update query");
		}

		//String[] cols = sql.substring(sql.indexOf('(') + 1, sql.indexOf(')')).split(",");
		for (String col : cols) {
			String fieldName = getField(col);
			for (Method method : obj.getClass().getMethods()) {
				if (!method.getName().toLowerCase().startsWith("set")) {
					if (method.getName().toLowerCase().trim().equals("get" + fieldName.toLowerCase().trim()) || method.getName().toLowerCase().trim().equals("is" + fieldName.toLowerCase().trim())) {
						try {
							param.add(method.invoke(obj) == null ? null : method.invoke(obj));
						} catch (Exception e) {
							LOGGER.error("Exception occured during " + operation + " operation.", e);
						}
					}
				}
			}
		}

		Object[] params = param.toArray(new Object[param.size() - 1]);
		try {
			return new QueryRunner().update(con, sql, params);
		} catch (SQLException e) {
			LOGGER.error("Exception in executeInsertUpdate", e);
			throw new DbOperationException(e);
		}
	}

	
	
	public <T> int[] executeInsertUpdateBatch(String sql, List<T> objs,Connection connection) throws Exception {

		String[] cols = sql.substring(sql.indexOf('(') + 1, sql.indexOf(')')).split(",");
		List<Object[]> multiParams = new ArrayList<>();
		for (int i = 0; i < objs.size(); i++) {
			List<Object> param = new ArrayList<>();
			Object obj = objs.get(i);
			for (String col : cols) {
				String fieldName = getField(col);
				for (Method method : obj.getClass().getMethods()) {
					if (method.getName().toLowerCase().trim().equals("get" + fieldName.toLowerCase().trim()) || method.getName().toLowerCase().trim().equals("is" + fieldName.toLowerCase().trim())) {
						try {
							param.add(method.invoke(obj));
							break;
						} catch (Exception e) {
							LOGGER.error("Exception occured during insert operation.", e);
						}
					}
				}
			}
			multiParams.add(param.toArray(new Object[param.size() - 1]));
		}

		try {
			return new QueryRunner().batch(connection, sql, multiParams.toArray(new Object[multiParams.size()][]));
		} catch (SQLException e) {
			LOGGER.error("Exception in executeInsertUpdateBatch", e);
			throw new Exception(e);
		}
	}


}