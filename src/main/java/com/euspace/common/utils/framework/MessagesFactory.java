package com.euspace.common.utils.framework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.euspace.common.utils.MessageItem;
import com.euspace.common.utils.MessagesUtils;


public class MessagesFactory {

	private MessagesFactory() {
		// Unused
	}

	public static Map<String, List<MessageItem>> generalMessageMapWithCode(String messageCode, Locale locale,String statusCode) {
		Map<String, List<MessageItem>> errors = new HashMap<>();
		List<MessageItem> items = new ArrayList<>();

		MessageItem item = new MessageItem(messageCode, MessagesUtils.messageForKey(messageCode, locale));
		items.add(item);

		errors.put("general", items);
		return errors;
	}

	public static Map<String, List<MessageItem>> generalMessageMapForStringReplacement(String messageCode, String message,String statusCode) {
		Map<String, List<MessageItem>> errors = new HashMap<>();
		List<MessageItem> items = new ArrayList<>();

		MessageItem item = new MessageItem(messageCode, message);
		items.add(item);

		errors.put("general", items);
		return errors;
	}
}