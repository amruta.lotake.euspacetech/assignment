package com.euspace.common.utils;

import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import com.euspace.common.model.ApiSession;


public class HttpUtils {

	//public static final String REQUEST_METHOD_OPTIONS = "OPTIONS";

	public static final String REQUEST_HEADER_AUTHORIZATION = "Authorization";

	public static final String AUTHORIZATION_HEADER_PREFIX = "Bearer ";

	//public static final String PATH_PARAM_TENANT_ID = "tenantid";

	public static final String SESSION_DATA = "sessionData";

	private HttpUtils() {
		// Unused
	}

	public static Locale localeForRequest(String acceptLanguageHeader) {

		//String acceptLanguageHeader = request.getHeaders("reqeust");

		if (acceptLanguageHeader == null) {
			return new Locale("en", "US");
		}

		acceptLanguageHeader = acceptLanguageHeader.trim();

		if ("*".equals(acceptLanguageHeader)) {
			return new Locale("en", "US");
		}

		int indexOfComma = acceptLanguageHeader.indexOf(',');

		String toProcessFurther = acceptLanguageHeader;

		if (indexOfComma >= 0) {
			if (indexOfComma == 0) {
				return new Locale("en", "US");
			}

			toProcessFurther = acceptLanguageHeader.substring(0, indexOfComma);
		}

		if (toProcessFurther.length() == 2) {
			return new Locale(toProcessFurther);
		}

		if ((toProcessFurther.length() == 5) && (toProcessFurther.indexOf('_') == 2)) {
			String[] parts = toProcessFurther.split("_");
			return new Locale(parts[0], parts[1]);
		} else {
			return new Locale("en", "US");
		}
	}

	/*public static Optional<String> getParam(Request request, String paramName) {
		String paramValue = request.params(paramName);
		if (paramValue != null && paramValue.length() > 0) {
			return Optional.of(paramValue);
		}
		return Optional.empty();
	}
	
	public static Optional<String> getQueryParam(Request request, String paramName) {
		String paramValue = request.queryParams(paramName);
		if (paramValue != null && paramValue.length() > 0) {
			return Optional.of(paramValue);
		}
		return Optional.empty();
	}*/

	public static Optional<String> getJwtTokenFromHeader(String authorizationHeader) {
		if (authorizationHeader == null || "".equals(authorizationHeader)) {
			return Optional.empty();
		}
		String jwtToken = authorizationHeader.replace(AUTHORIZATION_HEADER_PREFIX, "");

		if (jwtToken.equals("Bearer"))
			return Optional.empty();
		return Optional.of(jwtToken);
	}

	/*public static String getMatchedUrl(Request request) {
		String apiUrl = request.uri();
		Map<String, String> params = request.params();
		for (Map.Entry<String, String> entry : params.entrySet()) {
			apiUrl = apiUrl.replace(entry.getValue(), entry.getKey());
		}
		return apiUrl;
	}*/

	public static String getSessionuser(HttpSession httpSession) {
		ApiSession sessionData = (ApiSession) httpSession.getAttribute(SESSION_DATA);
		return sessionData.getUserId();
	}

}
