package com.euspace.common.utils;

import java.security.SecureRandom;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

	private static final int OTP_LENGTH = 6;
	
	private static final int INVOICE_CODE_LENGTH = 6;
	
	private StringUtils() {
		// Unused
	}

	public static boolean isEmpty(String str) {
		return str == null || str.isEmpty();
	}

	public static String getNullAsString(Object obj) {
		return obj != null ? obj.toString() : "";
	}

	public static String extractBearerToken(String token) {
		if (token.isEmpty()) {
			return token;
		}
		return token.replaceFirst(Constants.AUTH_TYPE_BEARER, "").trim();
	}

	public static String generateVerificationCode(boolean constantOtp) {

		if (constantOtp) {
			return "1234";
		}
		StringBuilder str = new StringBuilder();
		SecureRandom srn = new SecureRandom();
		for (int i = 0; i < 4; i++) {
			str.append(srn.nextInt(10));
		}
		return str.toString();
	}

	public static String getOTP() {

		String otpNumbers = "0123456789" + "9876543210";
		Random rnd = new SecureRandom();
		StringBuilder otp = new StringBuilder();
		for (int i = 0; i < OTP_LENGTH; i++) {
			otp.append(otpNumbers.charAt(rnd.nextInt(otpNumbers.length())));
		}
		return otp.toString();

	}

	public static String surroundWithSqlWildCard(String search) {
		if (search != null) {
			return "%" + search.replace("%", "\\%") + "%";
		}
		return search;
	}

	public static String getFileExtension(String fileName) {
		int lastIndexOfDot = fileName.lastIndexOf('.');
		if (lastIndexOfDot == -1) {
			return "";
		}
		return fileName.substring(lastIndexOfDot);
	}

	public static boolean isPhoneNumberValid(String phoneNumber) {
		boolean isValid = false;

		String expression = "\\d{10}";
		CharSequence inputStr = String.valueOf(phoneNumber);
		Pattern pattern = Pattern.compile(expression);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}
	
	public static String getInvoiceNumber() {

		String randonNumberString = "0123456789" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "9876543210" + "ZYXWVUTSRQPONMLKJIHGFEDCBA";
		Random rnd = new SecureRandom();
		StringBuilder referralCode = new StringBuilder();
		for (int i = 0; i < INVOICE_CODE_LENGTH; i++) {
			referralCode.append(randonNumberString.charAt(rnd.nextInt(randonNumberString.length())));
		}
		return referralCode.toString();

	}
}
