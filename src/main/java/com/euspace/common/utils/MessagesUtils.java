package com.euspace.common.utils;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class MessagesUtils {

	private static final String MESSAGES_FILE_NAME = "messages";

	private static Map<Locale, ResourceBundle> bundles = new HashMap<>();

	private MessagesUtils() {
		// Do nothing
	}

	public static String messageForKey(String key, Locale locale) {
		ResourceBundle messages = bundles.get(locale);

		if (messages == null) {
			messages = ResourceBundle.getBundle(MESSAGES_FILE_NAME, locale);
			bundles.put(locale, messages);
		}

		return messages.getString(key);
	}
}
