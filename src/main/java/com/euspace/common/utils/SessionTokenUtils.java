package com.euspace.common.utils;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.euspace.common.model.ApiSession;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class SessionTokenUtils {

	private static final Logger LOG = LoggerFactory.getLogger(SessionTokenUtils.class);

	private static final String KEY_USER_ID = "USER_ID";

	private SessionTokenUtils() {
		// Unused
	}

	public static Optional<String> getToken(ApiSession data) {
		try {
			String jwt = Jwts.builder().setSubject("users/TzMUocMF4p").setExpiration(new Date()).claim(KEY_USER_ID, data.getUserId()).signWith(SignatureAlgorithm.HS256, "secret".getBytes("UTF-8")).compact();
			UUID uuid = UUID.randomUUID();
			String uuidString = uuid.toString().replace("-", "");
			String finalToken = jwt + uuidString;
			return Optional.of(finalToken);
		} catch (Exception e) {
			LOG.error("Unsupported exception occured while getToken", e);
			return Optional.empty();
		}
	}

	public static Optional<ApiSession> verify(String token) throws UnsupportedEncodingException {
		try {
			DecodedJWT jwt = JWT.decode(token);
			if (jwt != null) {
				ApiSession data = new ApiSession();
				data.setUserId(jwt.getClaim(KEY_USER_ID).asString());
				//data.setTenantId(jwt.getClaim(KEY_TENANT_ID).asString());
				return Optional.of(data);
			}

			return Optional.empty();
		} catch (JWTVerificationException exception) {
			LOG.error("Invalid jwt token" + exception);
			return Optional.empty();
		}
	}
}
