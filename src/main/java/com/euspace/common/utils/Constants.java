package com.euspace.common.utils;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Constants {

	private Constants() {
		super();
	}

	public static final String GENERAL_MESSAGE_KEY = "general";
	public static final String FIELD_ERROR = "fieldError";

	public static final String EMAIL_PATTERN = "[A-Za-z0-9._%-+]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";

	public static final int ROLE_ADMIN_ID = 1;
	public static final int ROLE_USER_ID = 2;
	public static final int ROLE_WORKER_ID = 3;

	public static final String ROLE_ADMIN_STR = "ADMIN";
	public static final String ROLE_USER_STR = "USER";
	public static final String ROLE_WORKER_STR = "WORKER";

	public static final String ROLE_CODE_ADMIN_STR = "ADMIN_1";
	public static final String ROLE_CODE_USER_STR = "USER_2";
	public static final String ROLE_CODE_WORKER_STR = "WORKER_3";

	public static final String ACCESS_USER = "USER_ACCESS";
	public static final String ACCESS_ADMIN = "ADMIN_ACCESS";

	public static final String AUTH_HEADER_NAME = "Authorization";
	public static final String AUTH_TYPE_BEARER = "Bearer";
	public static final long TEN_DAYS = 1000L * 60 * 60 * 24 * 10;

	public static class AppVersion {

		public static final String NO_NEW_RELEASE = "NO_NEW_RELEASE";
		public static final String UNSUPPORTED_RELEASE = "UNSUPPORTED_RELEASE";
		public static final String MANDATORY_RELEASE = "MANDATORY_RELEASE";
		public static final String OPTIONAL_RELEASE = "OPTIONAL_RELEASE";

		private AppVersion() {
			// Not to be instantiated
		}
	}

	public static class DeviceTypes {

		public static final String IOS = "ios";
		public static final String ANDROID = "android";

		public static final List<String> SUPPORTED = Collections.unmodifiableList(Arrays.asList(IOS, ANDROID));

		private DeviceTypes() {
			// Not to be instantiated
		}
	}

	public static class AppTypes {

		public static final String CUSTOMER = "customer";

		public static final List<String> SUPPORTED = Collections.unmodifiableList(Arrays.asList(CUSTOMER));

		public static String appTypesAsStr() {
			StringBuilder b = new StringBuilder();
			SUPPORTED.forEach(new StringBuilder()::append);
			return b.toString();
		}

		private AppTypes() {
			// Not to be instantiated
		}
	}

	public static final List<String> KNOWN_EXCEPTIONS = Collections.unmodifiableList(Arrays.asList("org.springframework.context.NoSuchMessageException", "org.springframework.web.servlet.NoHandlerFoundException", "org.springframework.web.bind.MethodArgumentNotValidException",
			"org.springframework.web.HttpRequestMethodNotSupportedException", "org.springframework.jdbc.BadSqlGrammarException", "org.springframework.security.authentication.AuthenticationCredentialsNotFoundException",
			"com.fasterxml.jackson.databind.JsonMappingException", "com.fasterxml.jackson.core.JsonParseException", "com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException"));

	public static final String OTP_NOT_FOUND = "otp not found";
	public static final String OTP_MATCHED = "otp matched";
	public static final String OTP_EXPIRED = "otp expired";

	public static final String ITEM_SORT_TYPE_HIGH_TO_LOW = "highToLow";
	public static final String ITEM_SORT_TYPE_LOW_TO_HIGH = "lowToHigh";
	public static final String ITEM_SORT_TYPE_ALPHABETICAL = "alphabetical";

	public static final String ITEM_REFINE_BY_LT20 = "lt20";
	public static final String ITEM_REFINE_BY_21TO50 = "21to50";
	public static final String ITEM_REFINE_BY_50TO100 = "50to100";
	public static final String ITEM_REFINE_BY_GT100 = "gt100";

	public static final String TEMP_DIR_PATH = "E:\\testFiles\\";

//	public static final String TEMP_DIR_PATH = "/var/tmp/";
	

	public static final String S3BUCKET_NAME = "euspace-image";

	public static final String BOOKING_ADD_STATUS = "Pending";
	public static final String BOOKING_UPDATE_STATUS = "Completed";

	public static final String CART_NEW_REQUEST_STATUS = "New Request";
	public static final String CART_COMPLETED_STATUS = "Completed";

	public static final String CART_ADD_ACTION = "plus";
	public static final String CART_SUBSTRACT_ACTION = "minus";

	public static final String EMPTY_STRING = "";
	
	public static final String BOOKING_CANCELLED_STATUS = "Cancelled";
	
	public	static final LocalTime TIME_12_AM = LocalTime.parse( "00:00:00"  ) ;
	public static final LocalTime TIME_5_AM = LocalTime.parse( "05:00:00" ) ;
	public static final LocalTime TIME_5_01_AM = LocalTime.parse( "05:00:01" ) ;
	public static final LocalTime TIME_4_PM = LocalTime.parse( "16:00:00" ) ;
	public static final LocalTime TIME_4_01_Pm = LocalTime.parse( "16:00:01" ) ;
	public static final LocalTime TIME_11_PM = LocalTime.parse( "23:59:59" ) ;
	
	public static final String Morning_Time_Slot = "08:00 AM To 11:00 AM";
	public static final String Eveneing_Time_Slot = "06:00 PM To 09:00 PM";
	
	public static final String Morning = "Morning";
	public static final String Eveneing = "Eveneing";
	
	public static final String Express_Time = "60 min";
	public static final double EXPRESS_AMOUNT = 50.00;
}
