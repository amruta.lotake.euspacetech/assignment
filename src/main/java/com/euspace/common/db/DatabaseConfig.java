package com.euspace.common.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DatabaseConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseConfig.class);


	@Value("${spring.datasource.url}")
	private String dbURL;

	@Value("${spring.datasource.driver}")
	private String driverClassName;

	@Value("${spring.datasource.username}")
	private String username;

	@Value("${spring.datasource.password}")
	private String password;

	@Value("${DB_MIN_IDLE_CONNS}")
	private int minIdleConns;

	@Value("${DB_MAX_POOL_SIZE}")
	private int maximumPoolSize;



	@Bean
	public HikariConfig hikariConfig() {

		LOGGER.info("DB URL : {}  ");

		HikariConfig hikariConfig = new HikariConfig();
		hikariConfig.setDriverClassName(driverClassName);
		hikariConfig.setJdbcUrl(dbURL);
		hikariConfig.setUsername(username);
		hikariConfig.setPassword(password);
		if (minIdleConns > 0) {
			hikariConfig.setMinimumIdle(minIdleConns);
		}
		if (maximumPoolSize > 0) {
			hikariConfig.setMaximumPoolSize(maximumPoolSize);
		}
		return hikariConfig;
	}

	@Bean
	public HikariDataSource getDataSource() {
		return new HikariDataSource(hikariConfig());
	}

	@Bean
	public DataSourceTransactionManager transactionManager() {
		return new DataSourceTransactionManager(getDataSource());
	}


}
