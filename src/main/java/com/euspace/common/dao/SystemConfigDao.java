package com.euspace.common.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.euspace.common.model.SystemConfig;

@Component
public interface SystemConfigDao {

	public List<SystemConfig> fetchAllEnvConfig(String env);
}