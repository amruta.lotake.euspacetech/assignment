package com.euspace.common.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;
import com.euspace.common.dao.AppVersionDao;
import com.euspace.common.model.AppVersion;
import com.euspace.common.model.AppVersionReleaseNote;

@Repository
public class AppVersionDaoImpl implements AppVersionDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public AppVersion retriveLatestVersion(String appType, String deviceType) {
		String sql = "SELECT * FROM app_versions WHERE app_type = ? AND device_type = ? AND is_deleted=false ORDER BY release_date desc LIMIT 1";

		Object[] params = new Object[] { appType,  deviceType};

		AppVersion appVersion = jdbcTemplate.query(sql, new ResultSetExtractor<AppVersion>() {
			public AppVersion extractData(ResultSet rs) throws SQLException, DataAccessException {

				AppVersion appVersion = new AppVersion();
				while (rs.next()) {

					appVersion.setAppVersionId(rs.getString("app_version_id"));
					appVersion.setVersionNo(rs.getString("app_version_id"));
					appVersion.setAppType(rs.getString("app_version_id"));
					appVersion.setDeviceType(rs.getString("app_version_id"));
					appVersion.setAppLink(rs.getString("app_version_id"));
					appVersion.setReleaseDate(rs.getLong("release_date"));
					appVersion.setMandatory(rs.getBoolean("is_mandatory"));
					appVersion.setRemoveSupportFrom(rs.getLong("remove_support_from"));
					 
					return appVersion;
				}

				return appVersion;
			}
		}, params);

		return appVersion;
	}

	@Override
	public boolean checkMandatoryReleaseAvailable(String appType, String deviceType, long releaseDate) {
		String sql = "SELECT count(app_version_id) > 0 FROM app_versions WHERE app_type = ? AND device_type = ? AND release_date > ? AND is_deleted = false AND is_mandatory =true";
		Object[] params = new Object[] { appType,  deviceType, releaseDate};
		
		int result = jdbcTemplate.queryForObject(sql,params, Integer.class);

		if (result > 0) {
			return true;
		}

		return false;
		
	}

	@Override
	public AppVersion retriveVersionByVersion(String appType, String deviceType, String versionNo) {
		String sql = "SELECT * FROM app_versions WHERE app_type = ? AND device_type = ? AND version_no = ? AND is_deleted=false ";

		Object[] params = new Object[] { appType,  deviceType};

		AppVersion appVersion = jdbcTemplate.query(sql, new ResultSetExtractor<AppVersion>() {
			public AppVersion extractData(ResultSet rs) throws SQLException, DataAccessException {

				AppVersion appVersion = new AppVersion();
				while (rs.next()) {

					appVersion.setAppVersionId(rs.getString("app_version_id"));
					appVersion.setVersionNo(rs.getString("app_version_id"));
					appVersion.setAppType(rs.getString("app_version_id"));
					appVersion.setDeviceType(rs.getString("app_version_id"));
					appVersion.setAppLink(rs.getString("app_version_id"));
					appVersion.setReleaseDate(rs.getLong("release_date"));
					appVersion.setMandatory(rs.getBoolean("is_mandatory"));
					appVersion.setRemoveSupportFrom(rs.getLong("remove_support_from"));
					 
					return appVersion;
				}

				return appVersion;
			}
		}, params);

		return appVersion;
	}

	@Override
	public List<AppVersionReleaseNote> retriveReleaseNotesForVersion(String appVersionId) {
		
		String sql = "SELECT * FROM app_version_release_notes WHERE app_version_id = ? AND is_deleted=false ";
		Object[] params = new Object[] { appVersionId};
		
		List<AppVersionReleaseNote> appVersionReleaseNoteList = jdbcTemplate.query(sql, params, new ResultSetExtractor<List<AppVersionReleaseNote>>() {
			@Override
			public List<AppVersionReleaseNote> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<AppVersionReleaseNote> list = new ArrayList<AppVersionReleaseNote>();
				while (rs.next()) {
					AppVersionReleaseNote appVersionReleaseNote = new AppVersionReleaseNote();

					appVersionReleaseNote.setAppVersionReleaseNoteId(rs.getString("app_version_release_id"));
					appVersionReleaseNote.setAppVersionId(rs.getString("app_version_id"));
					appVersionReleaseNote.setLanguageCode(rs.getString("language_code"));
					appVersionReleaseNote.setReleaseNotes(rs.getString("release_notes"));

					list.add(appVersionReleaseNote);
				}
				return list;
			}

		});
		return appVersionReleaseNoteList;
	}

}
