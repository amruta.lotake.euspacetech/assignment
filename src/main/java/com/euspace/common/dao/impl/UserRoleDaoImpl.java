package com.euspace.common.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.euspace.common.dao.UserRoleDao;
import com.euspace.common.model.UserRoleModel;

@Repository
public class UserRoleDaoImpl implements UserRoleDao{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public int grantRoles(List<UserRoleModel> roles) {
		String sql = "INSERT INTO user_roles (user_role_id, user_id,role_id,is_active,"
				+ "created_at, updated_at, created_by, updated_by)"
				+ " values (?, ?, ?, ?, ?, ?, ?, ?)";

		List<Object[]> inputList = new ArrayList<Object[]>();
        for(UserRoleModel userRoleModel:roles){
            Object[] tmp = {userRoleModel.getUserRoleId(),userRoleModel.getUserId(),userRoleModel.getRoleId(),userRoleModel.isActive(),userRoleModel.getCreatedAt(),userRoleModel.getUpdatedAt(),userRoleModel.getCreatedBy(),userRoleModel.getUpdatedBy()};
            inputList.add(tmp);
        }
        int[] result = jdbcTemplate.batchUpdate(sql, inputList);
		return result.length;
		 
	}

	@Override
	public Set<String> retriveUserRoles(String userId) {
		
		String sql = "SELECT R.role_id FROM user_roles UR" + 
					" INNER JOIN roles R ON UR.role_id = R.role_id "+
					" WHERE UR.user_id = ?";
			
		String result =  this.jdbcTemplate.queryForObject(
                sql, new Object[] { userId }, String.class);
		Set<String> res =new HashSet<>();
		res.add(result);
		
		return res;
	}
	
}
