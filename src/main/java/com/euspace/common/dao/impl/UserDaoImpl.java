package com.euspace.common.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import com.euspace.common.dao.UserDao;
import com.euspace.common.dto.UserAddDto;
import com.euspace.common.dto.UserDto;
import com.euspace.common.model.Role;
import com.euspace.common.model.User;
import com.euspace.common.utils.DateUtils;

@Repository
public class UserDaoImpl implements UserDao {
	protected static final boolean STATUS_NOT_DELETED = false;
	private static final Logger LOG = LoggerFactory.getLogger(UserDaoImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Optional<User> findByUsername(String username) {
		String sql = "SELECT * FROM users WHERE username = ? AND is_active = ?";

		Object[] params = new Object[] { username, true };

		User user = jdbcTemplate.query(sql, new ResultSetExtractor<User>() {
			public User extractData(ResultSet rs) throws SQLException, DataAccessException {

				User userResult = new User();
				while (rs.next()) {

					User user = new User();

					user.setUserId(rs.getString("user_id"));
					user.setFirstName(rs.getString("first_name"));
					user.setLastName(rs.getString("last_name"));
					user.setEmail(rs.getString("email"));
					user.setPhoneNumber(rs.getString("phone_number"));
					user.setDateOfBirth(rs.getLong("date_of_birth"));
					user.setProfileImage(rs.getString("profile_image"));
					user.setCredentialsExpired(rs.getBoolean("credentials_expired"));
					user.setAccountEnabled(rs.getBoolean("account_enabled"));
					user.setAccountExpired(rs.getBoolean("account_expired"));
					user.setAccountLocked(rs.getBoolean("account_locked"));
					user.setActive(rs.getBoolean("is_active"));
					user.setCreatedBy(rs.getString("created_by"));
					user.setCreatedAt(rs.getLong("created_at"));
					user.setUpdatedBy(rs.getString("updated_by"));
					user.setUpdatedAt(rs.getLong("updated_at"));
					user.setDeleted(rs.getBoolean("deleted"));

					return user;
				}

				return userResult;
			}
		}, params);

		return Optional.ofNullable(user);
	}

	@Override
	public boolean userExists(String username) {
		String sql = "SELECT count(user_id) FROM users WHERE username = ? AND is_active = ?";
		Object[] params = new Object[] { username, true };

		int result = jdbcTemplate.queryForObject(sql, params, Integer.class);

		if (result > 0) {
			return true;
		}

		return false;
	}

	@Override
	public boolean emailExists(String email) {
		String sql = "SELECT count(user_id) FROM users WHERE email = ? AND is_active = ?";

		Object[] params = new Object[] { email, true };

		int result = jdbcTemplate.queryForObject(sql, params, Integer.class);

		if (result > 0) {
			return true;
		}

		return false;
	}

	@Override
	public User findById(String userId) {
		String sql = "SELECT * FROM users WHERE user_id = ? AND is_active = ?";

		Object[] params = new Object[] { userId, true };

		User user = jdbcTemplate.query(sql, new ResultSetExtractor<User>() {
			public User extractData(ResultSet rs) throws SQLException, DataAccessException {

				User user = new User();
				while (rs.next()) {

					user.setUserId(rs.getString("user_id"));
					user.setFirstName(rs.getString("first_name"));
					user.setLastName(rs.getString("last_name"));
					user.setEmail(rs.getString("email"));
					user.setPhoneNumber(rs.getString("phone_number"));
					user.setDateOfBirth(rs.getLong("date_of_birth"));
					user.setProfileImage(rs.getString("profile_image"));
					user.setCredentialsExpired(rs.getBoolean("credentials_expired"));
					user.setAccountEnabled(rs.getBoolean("account_enabled"));
					user.setAccountExpired(rs.getBoolean("account_expired"));
					user.setAccountLocked(rs.getBoolean("account_locked"));
					user.setActive(rs.getBoolean("is_active"));
					user.setCreatedBy(rs.getString("created_by"));
					user.setCreatedAt(rs.getLong("created_at"));
					user.setUpdatedBy(rs.getString("updated_by"));
					user.setUpdatedAt(rs.getLong("updated_at"));
					user.setDeleted(rs.getBoolean("deleted"));

					return user;
				}

				return user;
			}
		}, params);

		return user;
	}

	@Override
	public int addUser(User user) {
		String sql = "INSERT INTO users ( user_id,  password, email, first_name,last_name,"
				+ "date_of_birth,phone_number,profile_image,credentials_expired, "
				+ "account_enabled, account_expired, account_locked"
				+ ",created_at,created_by,updated_at,updated_by,is_active,deleted)" 
				+ " values "
				+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		return jdbcTemplate.update(sql, user.getUserId(), user.getPassword(), user.getEmail(), user.getFirstName(), 
				user.getLastName(), user.getDateOfBirth(), user.getPhoneNumber(),  user.getProfileImage(),
				user.isCredentialsExpired(), user.isAccountEnabled(), user.isAccountExpired(),
				user.isAccountLocked(), user.getCreatedAt(), user.getCreatedBy(), user.getUpdatedAt(), 
				user.getUpdatedBy(), user.isActive(), user.isDeleted());
	}
	@Override
	public boolean isPhoneNumberExistsForOtherUsers(String phoneNumber, String userId) {
		// @formatter:off
		String sql = "SELECT (COUNT(*)>0) "
				+ "FROM"
				+ " users "
				+ "WHERE"
				+ " phone_number= ? "
				+ "AND"
				+ " user_id != ? "
				+ "AND"
				+ " deleted =  ?";
		// @formatter:on
		boolean status = jdbcTemplate.queryForObject(sql, new Object[] { phoneNumber, userId, false }, Boolean.class);
		return status;
	}

	@Override
	public boolean isEmailExistsForOtherUsers(String email, String userId) {
		// @formatter:off
		String sql = "SELECT (COUNT(*)>0) "
				+ "FROM"
				+ " users "
				+ "WHERE"
				+ " email= ? "
				+ "AND"
				+ " user_id != ? "
				+ "AND"
				+ " deleted =  ?";
		// @formatter:on
		boolean status = jdbcTemplate.queryForObject(sql, new Object[] { email, userId, false }, Boolean.class);
		return status;
	}

		
	@Override
	public Optional<List<Role>> getUserRoles(String userId) {
		// @formatter:off
		String sql = "SELECT R.role_id,R.role_name,R.role_code "
				+ "FROM"
				+ " roles R "
				+ "INNER JOIN "
				+ " user_roles UR "
				+ "ON"
				+ " R.role_id=UR.role_id "
				+ "WHERE"
				+ " UR.user_id=?";
		// @formatter:on
		Object[] params = new Object[] { userId };

		List<Role> roleList = jdbcTemplate.query(sql, new ResultSetExtractor<List<Role>>() {
			@Override
			public List<Role> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Role> list = new ArrayList<Role>();

				while (rs.next()) {

					Role role = new Role();

					role.setRoleId(rs.getInt("role_id"));
					role.setRoleName(rs.getString("role_name"));
					role.setRoleCode(rs.getString("role_code"));
					list.add(role);
				}
				return list;
			}

		}, params);

		if (!roleList.isEmpty()) {
			return Optional.of(roleList);
		}

		return Optional.empty();
	}
	
	@Override
	public Optional<UserDto> getUserCompleteDetailsById(String userId) {

		// @formatter:off
		String sql = "SELECT  user_id,email,first_name,"
				+ "last_name,date_of_birth,phone_number,"
				+ "profile_image,is_active,deleted "
				+ " FROM " 
				+ " users "
				+ "WHERE"
				+ " user_id= ? "
				+ "AND"
				+ " deleted = ? ";
		
		Object[] params = new Object[] { userId, false };
		// @formatter:on
		UserDto user = jdbcTemplate.query(sql, new ResultSetExtractor<UserDto>() {
			public UserDto extractData(ResultSet rs) throws SQLException, DataAccessException {

				UserDto users = null;
				while (rs.next()) {

					users = new UserDto();
					users.setUserId(rs.getString("user_id"));
					users.setFirstName(rs.getString("first_name"));
					users.setLastName(rs.getString("last_name"));
					users.setEmail(rs.getString("email"));
					users.setPhoneNumber(rs.getString("phone_number"));
					String dateOfBirthStr = DateUtils.getFormattedDate(rs.getLong("date_of_birth"));
					users.setDateOfBirth(dateOfBirthStr);
					users.setProfileImage(rs.getString("profile_image"));
				}

				return users;
			}
		}, params);

		return Optional.ofNullable(user);
	}
	
	@Override
	public Optional<String> getUserPasswordByField(String field, String value) {
		// @formatter:off
		String sql = "SELECT password "
				+ "FROM"
				+ " users "
				+ "WHERE " 
				+ field + " = ?  "
				+ "AND"
				+ " deleted = ?";
		// @formatter:on
		Object[] params = new Object[] { value, STATUS_NOT_DELETED };

		String password = jdbcTemplate.query(sql, new ResultSetExtractor<String>() {

			public String extractData(ResultSet rs) throws SQLException, DataAccessException {
				String userPassword = " ";
				while (rs.next()) {
					userPassword = rs.getString("password");
				}

				return userPassword;
			}
		}, params);

		if (password != null && password.equals(""))
			return Optional.empty();
		return Optional.of(password);
	}
	
	@Override
	public Optional<UserDto> userDetailsByEmail(String field, String value) {
		// @formatter:off
		String sql = "SELECT  user_id,  password,"
				+ "email,first_name, last_name, date_of_birth, "
				+ "phone_number,  profile_image "  
				+ "FROM"
				+ " users "
				+ "WHERE "
				+  field + " = ? "
				+ "AND"
				+ " deleted = ?";
		// @formatter:on
		Object[] params = new Object[] { value, STATUS_NOT_DELETED };

		UserDto userInformationDetailsDto = jdbcTemplate.query(sql, new ResultSetExtractor<UserDto>() {
			public UserDto extractData(ResultSet rs) throws SQLException, DataAccessException {

				UserDto userAddDto = null;
				while (rs.next()) {
					userAddDto = new UserDto();
					userAddDto.setUserId(rs.getString("user_id"));
				    userAddDto.setPassword(rs.getString("password"));
					userAddDto.setEmail(rs.getString("email"));
					userAddDto.setFirstName(rs.getString("first_name"));
					userAddDto.setLastName(rs.getString("last_name"));
					String dateOfBirthStr = DateUtils.getFormattedDate(rs.getLong("date_of_birth"));
					userAddDto.setDateOfBirth(dateOfBirthStr);
					userAddDto.setPhoneNumber(rs.getString("phone_number"));
					userAddDto.setProfileImage(rs.getString("profile_image"));

				}
				return userAddDto;

			}
		}, params);

		return Optional.ofNullable(userInformationDetailsDto);
	}
	
	@Override
	public int deleteUser(int userId) {
		// @formatter:off
		     String sql = "UPDATE" 
				+ " users "
				+ "SET"
				+ " deleted = ?,"
				+ " updated_at = ?,"
				+ " updated_by = ? "
				+ "WHERE"
				+ " user_id = ? ";
		// @formatter:on		
		Object[] params = new Object[] { true, DateUtils.now(), userId, userId };

		return jdbcTemplate.update(sql, params);
	}
	
	@Override
	public int updateUser(UserDto userDto) {
		// @formatter:off
		     String sql = "UPDATE" 
				+ " users "
				+ "SET"
				+ " email = ?,"
				+ " first_name=?,"
				+ " last_name=?,"
				+ " date_of_birth=?,"
				+ " phone_number=? "
				+ "WHERE"
				+ " deleted = ?"
				+ " And "
				+ " userId = ? ";
		// @formatter:on
		     Long dateOfBirth = DateUtils.getTimestampFromDateString(userDto.getDateOfBirth());
			 
		     Object[] params = new Object[] {userDto.getEmail(),userDto.getFirstName(),
				userDto.getLastName(),dateOfBirth,userDto.getPhoneNumber(),
				false, userDto.getUserId()};

		return jdbcTemplate.update(sql, params);
	}
	
	@Override
	public Optional<List<UserDto>> getAllUserList() {
		// @formatter:off
		String sql = "SELECT  user_id,email,first_name,last_name,date_of_birth,"
				+ " phone_number,profile_image "
				+ "FROM"
				+ " users  "
				+ "WHERE"
				+ " deleted =  ?";

		// @formatter:on
		Object[] params = new Object[] {  STATUS_NOT_DELETED };
		List<UserDto> userList = jdbcTemplate.query(sql, params, new ResultSetExtractor<List<UserDto>>() {
			@Override
			public List<UserDto> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<UserDto> list = new ArrayList<UserDto>();
				while (rs.next()) {

					UserDto users = new UserDto();
					users.setUserId(rs.getString("user_id"));
					users.setEmail(rs.getString("email"));
					users.setFirstName(rs.getString("first_name"));
					users.setLastName(rs.getString("last_name"));
					String dateOfBirthStr = DateUtils.getFormattedDate(rs.getLong("date_of_birth"));
					users.setDateOfBirth(dateOfBirthStr);
					users.setPhoneNumber(rs.getString("phone_number"));
					users.setProfileImage(rs.getString("profile_image"));
					
				
					list.add(users);
				}

				return list;
			}
		});
		if (userList.isEmpty()) {
			return Optional.empty();
		}

		return Optional.of(userList);
	}

}
