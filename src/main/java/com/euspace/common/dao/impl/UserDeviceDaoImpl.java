package com.euspace.common.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;
import com.euspace.common.dao.UserDeviceDao;
import com.euspace.common.dto.UserAddDto;
import com.euspace.common.model.Role;
import com.euspace.common.model.UserDevice;
import com.euspace.common.utils.DateUtils;

@Repository
public class UserDeviceDaoImpl implements UserDeviceDao{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public int addUserDevice(UserDevice device) {
		String sql = "INSERT INTO user_devices (user_device_id, user_id, device_uid, device_token, device_name, device_type"
				+ " ,is_active, created_at, updated_at, created_by, updated_by)"
				+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		return jdbcTemplate.update(sql, device.getUserDeviceId(), device.getUserId(), device.getDeviceUid(), device.getDeviceToken(),
				device.getDeviceName(),device.getDeviceType(),
				device.isActive(),device.getCreatedAt(),device.getUpdatedAt(),device.getCreatedBy(),device.getUpdatedBy());
	}

	@Override
	public List<UserDevice> getUserDevices(String userId) {
		String sql = "SELECT * FROM user_devices WHERE userId = ? AND is_active=true ";
		Object[] params = new Object[] { userId};
		
		List<UserDevice> userDeviceList = jdbcTemplate.query(sql, params, new ResultSetExtractor<List<UserDevice>>() {
			@Override
			public List<UserDevice> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<UserDevice> list = new ArrayList<UserDevice>();
				while (rs.next()) {
					UserDevice userDevice = new UserDevice();
					
					userDevice.setDeviceUid(rs.getString("device_uid"));
					userDevice.setUserId(rs.getString("user_id"));
					userDevice.setDeviceUid(rs.getString("device_uid"));
					userDevice.setDeviceToken(rs.getString("device_token"));
					userDevice.setDeviceName(rs.getString("device_name"));
					userDevice.setDeviceType(rs.getString("device_type"));
					
					list.add(userDevice);
				}
				return list;
			}

		});
		return userDeviceList;
	}

	@Override
	public int deleteDeviec(String userId) {
		String query = "DELETE FROM user_devices WHERE user_id = ? AND is_active=true ";
		return jdbcTemplate.update(query, new Object[] { userId });
	}


	@Override
	public Optional<UserDevice> getUserDeviceDetails(String userId) {

		// @formatter:off
		
		String sql = "SELECT"
				+ " user_device_id,user_id,device_uid,device_token,device_name,"
				+ " device_type,created_at,created_by,updated_at,updated_by,is_active,deleted "
				+ "FROM"
				+ " user_devices "
				+ "WHERE"
				+ " user_id = ? "
				+ "AND"
				+ " is_active = ? "
				+ "AND"
				+ " deleted = ? ";
		
		Object[] params = new Object[] { userId, true, false };
		
		// @formatter:on

		UserDevice userDeviceInfo = jdbcTemplate.query(sql, new ResultSetExtractor<UserDevice>() {
			public UserDevice extractData(ResultSet rs) throws SQLException, DataAccessException {

				UserDevice userDeviceDetails = null;
				while (rs.next()) {

					userDeviceDetails = new UserDevice();
					userDeviceDetails.setUserDeviceId(rs.getString("user_device_id"));
					userDeviceDetails.setUserId(rs.getString("user_id"));
					userDeviceDetails.setDeviceUid(rs.getString("device_uid"));
					userDeviceDetails.setDeviceToken(rs.getString("device_token"));
					userDeviceDetails.setDeviceName(rs.getString("device_name"));
					userDeviceDetails.setDeviceType(rs.getString("device_type"));
					

				}
				return userDeviceDetails;

			}
		}, params);

		return Optional.ofNullable(userDeviceInfo);
	}

	

}
