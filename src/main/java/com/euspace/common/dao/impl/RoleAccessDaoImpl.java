package com.euspace.common.dao.impl;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;
import com.euspace.common.dao.RoleAccessDao;
import com.euspace.common.model.RoleAccess;

@Repository
public class RoleAccessDaoImpl implements RoleAccessDao{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<RoleAccess> fetchAllAccessForRole(int roleId) {
		String sql = "SELECT * FROM role_accesses WHERE role_id = ? AND is_active=true ";
		Object[] params = new Object[] { roleId};
		
		List<RoleAccess> roleAccessList = jdbcTemplate.query(sql, params, new ResultSetExtractor<List<RoleAccess>>() {
			@Override
			public List<RoleAccess> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<RoleAccess> list = new ArrayList<RoleAccess>();
				while (rs.next()) {
					RoleAccess roleAccess = new RoleAccess();
					
					roleAccess.setRoleAccessId(rs.getString("role_access_id"));
					roleAccess.setRoleId(rs.getString("role_id"));
					roleAccess.setAccessName(rs.getString("access_name"));
					roleAccess.setAccessGroup(rs.getString("access_group"));

					list.add(roleAccess);
				}
				return list;
			}

		});
		return roleAccessList;
	}

	@Override
	public boolean doesUserHasAccess(String accessName, String userId) {
		String sql = "SELECT count(role_access_id)>0 FROM role_accesses as RA "
				+ " INNER JOIN user_roles as UR ON UR.role_id=RA.role_id"
				+ " WHERE RA.is_active=true AND UR.is_active=true AND UR.user_id= ? AND RA.access_name=?";
		Object[] params = new Object[] { userId, accessName};
		
		int result = jdbcTemplate.queryForObject(sql,params, Integer.class);

		if (result > 0) {
			return true;
		}

		return false;
	}


}
