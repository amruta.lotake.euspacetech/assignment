package com.euspace.common.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;
import com.euspace.common.dao.ApiSessionDao;
import com.euspace.common.model.ApiSession;

@Repository
public class ApiSessionDaoImpl implements ApiSessionDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	@Override
	public int addSession(ApiSession session) {
		String sql = "insert into api_sessions (api_session_id, user_id,device_uid,expires_at, is_active, created_at,updated_at, created_by, updated_by)"
				+" values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		return jdbcTemplate.update(sql, session.getApiSessionId(), session.getUserId(), session.getDeviceUUID(), session.getExpiresAt(),session.isActive(),
				session.getCreatedAt(), session.getUpdatedAt(), session.getCreatedBy(), session.getUpdatedBy());

	}

	@Override
	public ApiSession fetchSession(String apiSessionId) {
		
		String sql = "SELECT * FROM api_sessions WHERE api_session_id = ? AND is_active=true";

		Object[] params = new Object[] { apiSessionId };

		ApiSession apiSession = jdbcTemplate.query(sql, new ResultSetExtractor<ApiSession>() {
			public ApiSession extractData(ResultSet rs) throws SQLException, DataAccessException {

				ApiSession apiSession = new ApiSession();
				while (rs.next()) {

					apiSession.setApiSessionId(rs.getString("api_session_id"));
					apiSession.setUserId(rs.getString("user_id"));
					apiSession.setDeviceUUID(rs.getString("device_uid"));
					apiSession.setExpiresAt(rs.getLong("expires_at"));
					apiSession.setActive(rs.getBoolean("is_active"));
					
					apiSession.setCreatedBy(rs.getString("created_by"));
					apiSession.setCreatedAt(rs.getLong("created_at"));
					apiSession.setUpdatedBy(rs.getString("updated_by"));
					apiSession.setUpdatedAt(rs.getLong("updated_at"));	
					 
					return apiSession;
				}

				return apiSession;
			}
		}, params);

		return apiSession;
	}

	@Override
	public int deleteSession(String apiSessionId) {
		String query = "DELETE FROM api_sessions WHERE api_session_id = ? AND is_active=true ";
		return jdbcTemplate.update(query, new Object[] { apiSessionId });
	}

}
