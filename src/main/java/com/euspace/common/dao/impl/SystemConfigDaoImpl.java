package com.euspace.common.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;
import com.euspace.common.dao.SystemConfigDao;
import com.euspace.common.model.SystemConfig;

@Repository
public class SystemConfigDaoImpl implements SystemConfigDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<SystemConfig> fetchAllEnvConfig(String env) {
		String sql = "SELECT * FROM system_configs WHERE environment = ? ";
		Object[] params = new Object[] { env};
		
		List<SystemConfig> systemConfigList = jdbcTemplate.query(sql, params, new ResultSetExtractor<List<SystemConfig>>() {
			@Override
			public List<SystemConfig> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<SystemConfig> list = new ArrayList<SystemConfig>();
				while (rs.next()) {
					SystemConfig systemConfig = new SystemConfig();
					
					systemConfig.setConfigKey(rs.getString("config_key"));
					systemConfig.setConfigValue(rs.getString("config_value"));
					systemConfig.setEnvironment(rs.getString("environment"));
					
					list.add(systemConfig);
				}
				return list;
			}

		});
		return systemConfigList;
	}

}
