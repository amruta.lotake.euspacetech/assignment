package com.euspace.common.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.euspace.common.dto.UserAddDto;
import com.euspace.common.model.Role;
import com.euspace.common.model.UserDevice;

@Component
public interface UserDeviceDao { 

	public int addUserDevice(UserDevice device);

	public List<UserDevice> getUserDevices(String userId);

	public int deleteDeviec(String userId);
		
	Optional<UserDevice> getUserDeviceDetails(String userId);
	

}