package com.euspace.common.dao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.euspace.common.model.RoleAccess;

@Component
public interface RoleAccessDao {

	public List<RoleAccess> fetchAllAccessForRole(int roleId);

	public boolean doesUserHasAccess(String accessName, String userId);
}