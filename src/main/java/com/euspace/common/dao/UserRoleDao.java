package com.euspace.common.dao;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;
import com.euspace.common.model.UserRoleModel;

@Component
public interface UserRoleDao {

	public int grantRoles(List<UserRoleModel> roles);
	
	public Set<String> retriveUserRoles(String userId);
}