package com.euspace.common.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.euspace.common.model.AppVersion;
import com.euspace.common.model.AppVersionReleaseNote;

@Component
public interface AppVersionDao {

	public AppVersion retriveLatestVersion(String appType, String deviceType);

	public boolean checkMandatoryReleaseAvailable(String appType, String deviceType, long releaseDate);

	public AppVersion retriveVersionByVersion(String appType, String deviceType, String versionNo);

	public List<AppVersionReleaseNote> retriveReleaseNotesForVersion(String appVersionId);

}
