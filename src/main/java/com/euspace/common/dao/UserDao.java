package com.euspace.common.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.euspace.common.dto.UserAddDto;
import com.euspace.common.dto.UserDto;
import com.euspace.common.model.Role;
import com.euspace.common.model.User;

@Component
public interface UserDao {

	public Optional<User> findByUsername(String username);

	public boolean userExists(String username);

	public boolean emailExists( String email);

	public User findById(String userId);

	public int addUser(User user);
	
	public boolean isPhoneNumberExistsForOtherUsers(String phoneNumber, String userId);

	public boolean isEmailExistsForOtherUsers(String email, String userId);
	
	Optional<List<Role>> getUserRoles(String id);
	
	Optional<UserDto> getUserCompleteDetailsById(String id);

	Optional<String> getUserPasswordByField(String field, String value);

	Optional<UserDto> userDetailsByEmail(String field, String value);

	int deleteUser(int id);
 
	int updateUser(UserDto userDto);

	Optional<List<UserDto>> getAllUserList();

}