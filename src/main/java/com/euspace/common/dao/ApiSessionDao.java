package com.euspace.common.dao;

import org.springframework.stereotype.Component;

import com.euspace.common.model.ApiSession;

@Component
public interface ApiSessionDao {


	public int addSession(ApiSession session);

	public ApiSession fetchSession( String apiSessionId);

	public int deleteSession(String apiSessionId);
}