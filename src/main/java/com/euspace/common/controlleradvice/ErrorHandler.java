package com.euspace.common.controlleradvice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.euspace.common.exception.AbstractBusinessException;
import com.euspace.common.exception.DBException;
import com.euspace.common.model.CustomErrorType;
import com.euspace.common.utils.Constants;
import com.euspace.common.utils.ErrorBuilder;
import com.euspace.common.utils.MessageItem;

@Order(Ordered.HIGHEST_PRECEDENCE)
@EnableWebMvc
@ControllerAdvice
@ComponentScan
public class ErrorHandler extends ResponseEntityExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(ErrorHandler.class);
	@Autowired
	private ErrorBuilder builder;

	@Value("${SEND_ACTUAL_ERROR}")
	private String sendActualError;

	@ExceptionHandler({ AccessDeniedException.class })
	public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex, WebRequest request) {
		return buildFinalMessage(ex, HttpStatus.FORBIDDEN, request);
	}

	private HttpStatus getStatus(HttpServletRequest request) {
		Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
		if (statusCode == null) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return HttpStatus.valueOf(statusCode);
	}

	@ExceptionHandler(value = { DataIntegrityViolationException.class, DBException.class })
	@ResponseBody
	ResponseEntity<Object> handleDBException(HttpServletRequest request, Exception ex) {
		HttpStatus status = getStatus(request);
		return buildFinalMessage(ex, status, request);
	}

	@ExceptionHandler(PSQLException.class)
	@ResponseBody
	ResponseEntity<Object> handlePSQLException(HttpServletRequest request, PSQLException ex) {
		HttpStatus status = getStatus(request);
		return buildFinalMessage(ex, status, request);
	}

	@ExceptionHandler(BadSqlGrammarException.class)
	@ResponseBody
	ResponseEntity<Object> handleBadSqlGrammarException(HttpServletRequest request, BadSqlGrammarException ex) {
		HttpStatus status = getStatus(request);
		return buildFinalMessage(ex, status, request);
	}

	@ExceptionHandler(AbstractBusinessException.class)
	@ResponseBody
	ResponseEntity<Object> handleBusinessException(HttpServletRequest request, AbstractBusinessException ex) {
		Map<String, List<MessageItem>> errors = builder.buildGeneral(ex.getErrroCode(), request.getLocale(), ex.getParams());
		return new ResponseEntity<>(new CustomErrorType(errors), HttpStatus.CONFLICT);
	}

	@ExceptionHandler(Exception.class)
	@ResponseBody
	ResponseEntity<Object> handleExceptionG(HttpServletRequest request, Throwable ex) {
		HttpStatus status = getStatus(request);
		return buildFinalMessage(ex, status, request);
	}

	@ExceptionHandler(Throwable.class)
	@ResponseBody
	ResponseEntity<Object> handleControllerException(HttpServletRequest request, Throwable ex) {
		HttpStatus status = getStatus(request);
		return buildFinalMessage(ex, status, request);
	}

	// Overriden
	@Override
	@ResponseBody
	protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		return buildFinalMessage(ex, status, request);
	}

	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		return buildFinalMessage(ex, status, request);
	}

	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
		logger.info("Sending bad request");
		return buildFinalMessage(ex, status, request);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

		List<FieldError> errors = ex.getBindingResult().getFieldErrors();
		Map<String, List<MessageItem>> response = new HashMap<>();
		if (!errors.isEmpty()) {
			response = builder.buildGeneral(errors.get(0).getCode(), request.getLocale());
		}

		for (FieldError fieldError : errors) {
			Map<String, List<MessageItem>> fieldErrorMessage = builder.buildFieldErrors(fieldError, request.getLocale());
			response.putAll(fieldErrorMessage);
		}
		return new ResponseEntity<>(new CustomErrorType(response), status);
	}

	private ResponseEntity<Object> buildFinalMessage(Throwable ex, HttpStatus status, WebRequest request, String... params) {

		Map<String, List<MessageItem>> errors = builder.buildGeneral(getExceptionMessage(ex), request.getLocale());
		return new ResponseEntity<>(new CustomErrorType(errors), status);
	}

	private ResponseEntity<Object> buildFinalMessage(Throwable ex, HttpStatus status, HttpServletRequest request) {
		Map<String, List<MessageItem>> errors = builder.buildGeneral(getExceptionMessage(ex), request.getLocale());
		return new ResponseEntity<>(new CustomErrorType(errors), status);
	}

	private String getExceptionMessage(Throwable ex) {
		logger.error("Throwing error : ", ex);
		logger.info("in get Exception : " + sendActualError + "   " + ex.getClass().getSimpleName() + "  : " + ex.getMessage());
		if ("yes".equalsIgnoreCase(sendActualError)) {
			return ex.getMessage();
		}
		String errorClass = ex.getClass().getCanonicalName();
		if (Constants.KNOWN_EXCEPTIONS.contains(errorClass)) {
			return errorClass;
		}
		logger.info(ex.getClass().getSimpleName());
		return "error.SomethingWentWrong";

	}

}