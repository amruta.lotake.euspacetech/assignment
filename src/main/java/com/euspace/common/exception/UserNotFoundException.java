package com.euspace.common.exception;

public class UserNotFoundException extends AbstractBusinessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3769027242637735105L;

	public UserNotFoundException(Object... params) {
		super("notFound.user", params);
	}

}
