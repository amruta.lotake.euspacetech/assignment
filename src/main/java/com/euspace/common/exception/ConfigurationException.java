package com.euspace.common.exception;

public class ConfigurationException extends RuntimeException {

	private static final long serialVersionUID = 7203257233758782157L;

	public ConfigurationException() {
		super();
	}

	public ConfigurationException(String message) {
		super(message);
	}

	public ConfigurationException(Throwable t) {
		super(t);
	}

}