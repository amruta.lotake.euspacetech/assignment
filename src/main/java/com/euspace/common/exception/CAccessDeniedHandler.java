package com.euspace.common.exception;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import com.euspace.common.model.CustomErrorType;
import com.euspace.common.utils.ErrorBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class CAccessDeniedHandler implements AccessDeniedHandler {

	@Autowired
	private ErrorBuilder builder;

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException arg2) throws IOException, ServletException {
		ObjectMapper mapper = new ObjectMapper();
		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		response.setContentType("application/json");
		CustomErrorType sessionError = new CustomErrorType(builder.buildGeneral("error.forbidden", request.getLocale()));
		response.getOutputStream().print(mapper.writeValueAsString(sessionError));
	}

}
