package com.euspace.common.exception;

import com.euspace.common.exception.AbstractBusinessException;

public class AppVersionException extends AbstractBusinessException {

	public AppVersionException(String errorCode, Object... params) {
		super(errorCode, params);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5442322113264570321L;

}
