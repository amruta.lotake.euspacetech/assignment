package com.euspace.common.exception;

public class DBException extends RuntimeException {

	private static final long serialVersionUID = -3477686497964395545L;

	private final String message;

	public DBException(String message) {
		super(message);
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}

}