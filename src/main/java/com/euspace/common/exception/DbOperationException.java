package com.euspace.common.exception;

public class DbOperationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DbOperationException() {
		super();
	}

	public DbOperationException(String message) {
		super(message);
	}

	public DbOperationException(Throwable t) {
		super(t);
	}
}
