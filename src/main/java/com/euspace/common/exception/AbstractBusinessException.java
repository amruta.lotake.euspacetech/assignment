package com.euspace.common.exception;

public abstract class AbstractBusinessException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7307019555957076455L;

	private final String errroCode;
	private final transient Object[] params;

	public AbstractBusinessException(String errroCode, Object... params) {
		super();
		this.errroCode = errroCode;
		this.params = params;
	}

	public String getErrroCode() {
		return errroCode;
	}

	public Object[] getParams() {
		return params;
	}

}
