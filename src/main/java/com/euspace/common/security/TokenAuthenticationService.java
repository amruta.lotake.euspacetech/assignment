package com.euspace.common.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.euspace.common.model.ApiSession;
import com.euspace.common.model.User;
import com.euspace.common.service.UserService;
import com.euspace.common.service.impl.ApiSessionServiceImpl;
import com.euspace.common.utils.Constants;
import com.euspace.common.utils.DateUtils;
import com.euspace.common.utils.StringUtils;

@Service
public class TokenAuthenticationService {
	@Autowired
	private UserService userService;

	@Autowired
	private ApiSessionServiceImpl sessionService;

	public void addAuthentication(HttpServletResponse response, UserAuthentication authentication) {
		long expire = DateUtils.now() + Constants.TEN_DAYS;
		final User user = authentication.getDetails();
		ApiSession session = new ApiSession(user.getUserId());
		session.setExpiresAt(expire);
		sessionService.addSession(session);
		user.setExpires(expire);
		String token = session.getApiSessionId();
		response.addHeader(Constants.AUTH_HEADER_NAME, Constants.AUTH_TYPE_BEARER + " " + token);
	}

	public Authentication getAuthentication(HttpServletRequest request) {

		final String token = request.getHeader(Constants.AUTH_HEADER_NAME);
		if (token == null || token.length() <= 0) {
			return null;
		}
		String sessionKey = StringUtils.extractBearerToken(token);
		ApiSession session = sessionService.fetchSession(sessionKey);
		if (session != null && session.getApiSessionId() != null) {
			User user = userService.getUserWithItsAccesses(session.getUserId());
			if (user != null) {
				user.setSessionKey(sessionKey);
				user.setExpires(session.getExpiresAt());
				return new UserAuthentication(user);
			}
		}
		return null;
	}

}