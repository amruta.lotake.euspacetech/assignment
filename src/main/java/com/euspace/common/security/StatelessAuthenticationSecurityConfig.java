package com.euspace.common.security;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.session.SessionManagementFilter;

import com.euspace.common.exception.CAccessDeniedHandler;
import com.euspace.common.utils.ErrorBuilder;

@EnableWebSecurity
@Configuration
@Order(1)
public class StatelessAuthenticationSecurityConfig extends WebSecurityConfigurerAdapter {

	private static final Logger LOGGER = LoggerFactory.getLogger(StatelessAuthenticationSecurityConfig.class);

	@Autowired
	private CustomCorsFilter customCorsFilter;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private TokenAuthenticationService tokenAuthenticationService;

	@Autowired
	private CAccessDeniedHandler accessDeniedHandler;

	@Autowired
	private UnauthorizedEntryPoint authenticationEntryPoint;

	@Autowired
	private ErrorBuilder errorBuilder;

	@Autowired
	private PublicUrlConfigurer publicUrlConfigurer;

	public StatelessAuthenticationSecurityConfig() {
		super(true);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// @formatter:off
		http.cors().and()
		.addFilterBefore(customCorsFilter, SessionManagementFilter.class)
		.addFilterBefore(new StatelessLoginFilter("/api/login", tokenAuthenticationService, userDetailsService, errorBuilder, authenticationManager()), UsernamePasswordAuthenticationFilter.class)
		.addFilterBefore(new StatelessAuthenticationFilter(tokenAuthenticationService, errorBuilder), UsernamePasswordAuthenticationFilter.class)
		.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint).accessDeniedHandler(accessDeniedHandler);
		// @formatter:on
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(HttpMethod.OPTIONS);
		web.ignoring().antMatchers(HttpMethod.GET, "/static/**");
		List<String[]> publicUrls = publicUrlConfigurer.getUrls();
		for (String[] url : publicUrls) {
			if (url.length != 2) {
				LOGGER.warn("Invalid public url config {}.", Arrays.toString(url));
				continue;
			}
			HttpMethod method = HttpMethod.resolve(url[0].toUpperCase());
			if (method != null) {
				web.ignoring().antMatchers(method, url[1]);
			} else {
				LOGGER.warn("Invalid public url config {}. Method name is not valid.", Arrays.toString(url));
			}

		}

	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
	}

	@Override
	protected UserDetailsService userDetailsService() {
		return userDetailsService;
	}

}