package com.euspace.common.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.euspace.common.model.User;
import com.euspace.common.service.UserService;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(UserDetailsService.class);

	private UserService userService;

	@Autowired(required = true)
	public UserDetailsService(UserService userRepo) {
		this.userService = userRepo;
	}

	private final AccountStatusUserDetailsChecker detailsChecker = new AccountStatusUserDetailsChecker();

	@Override
	public final User loadUserByUsername(String username) throws UsernameNotFoundException {
		final Optional<User> user = userService.findByUsername(username);

		if (!user.isPresent()) {
			logger.error("Sorry user not found : username--> " + username);
			throw new UsernameNotFoundException("user not found");
		}
		detailsChecker.check(user.get());
		return user.get();
	}

}