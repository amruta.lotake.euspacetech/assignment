package com.euspace.common.security;

import com.euspace.common.model.User;
import com.euspace.common.security.UserRole;

public enum UserRole {
	USER, ADMIN, WORKER, SYSTEM_ADMIN;

	public UserAuthority asAuthorityFor(final User user) {
		final UserAuthority authority = new UserAuthority();
		authority.setAuthority("ROLE_" + toString());
		authority.setUser(user);
		return authority;
	}

	public static final int ID_USER = 1;
	public static final int ID_ADMIN = 2;
	public static final int ID_WORKER=3;
	
	public static UserRole valueOf(final Integer i){
		switch (i) {
		case ID_USER:
			return UserRole.USER;
		case ID_ADMIN:
			return UserRole.ADMIN;
		case ID_WORKER:
			return UserRole.WORKER;
		default:
			throw new IllegalArgumentException("No role defined for roleId: " + i);
		}
	}
	
	
	public static UserRole valueOf(final UserAuthority authority) {
		switch (authority.getAuthority()) {
		case "ROLE_USER":
			return UserRole.USER;
		case "ROLE_ADMIN":
			return UserRole.ADMIN;
		case "ROLE_SYSTEM_ADMIN":
			return UserRole.SYSTEM_ADMIN;
		default:
			throw new IllegalArgumentException("No role defined for authority: " + authority.getAuthority());
		}

	}

}