package com.euspace.common.security;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class PublicUrlConfigurer {

	private List<String[]> urls = null;

	public PublicUrlConfigurer(List<String[]> urls) {
		this.urls = urls;
	}

	public List<String[]> getUrls() {
		return urls;
	}

}
