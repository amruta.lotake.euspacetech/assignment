package com.euspace.common.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;

public class CustomCorsFilter implements Filter {

	@Value("${cors.allowed.origins}")
	private String[] corsAllowedOrigins;

	@Value("${cors.allowed.headers}")
	private String corsAllowedHeaders;

	@Value("${cors.allowed.methods}")
	private String corsAllowedMethods;

	@Value("${cors.preflight.maxage}")
	private long corsPreflightMaxage;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// unused
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		if (corsAllowedOrigins.length == 1) {
			response.setHeader("Access-Control-Allow-Origin", corsAllowedOrigins[0]);
		} else {
			String requestOrigin = request.getHeader("Origin");
			if (requestOrigin != null) {
				for (String allowedO : corsAllowedOrigins) {
					if (allowedO.equalsIgnoreCase(requestOrigin)) {
						response.setHeader("Access-Control-Allow-Origin", allowedO);
						break;
					}
				}
			}
		}

		response.setHeader("Access-Control-Allow-Methods", corsAllowedMethods);
		response.setHeader("Access-Control-Allow-Headers", corsAllowedHeaders);
		response.setHeader("Access-Control-Max-Age", corsPreflightMaxage + "");
		filterChain.doFilter(servletRequest, servletResponse);
	}

	@Override
	public void destroy() {
		// unused
	}
}
