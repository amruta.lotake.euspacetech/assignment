package com.euspace.common.security;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.euspace.common.model.User;
import com.euspace.common.service.impl.RoleAccessServiceImpl;

@Component
public class MIPermissionEvaluator implements PermissionEvaluator {

	private static final Logger logger = LoggerFactory.getLogger(MIPermissionEvaluator.class);

	@Autowired
	private RoleAccessServiceImpl roleAccessService;

	@Override
	public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
		User user = (User) authentication.getDetails();
		boolean hasAccess = roleAccessService.doesUserHasAccess(permission.toString(), user.getUserId());
		logger.info("permission : " + permission + "  hasAccess:" + hasAccess);
		return hasAccess;
	}

	@Override
	public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
		return false;
	}

}