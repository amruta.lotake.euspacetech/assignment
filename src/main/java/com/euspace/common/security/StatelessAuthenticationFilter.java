package com.euspace.common.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import com.euspace.common.model.CustomErrorType;
import com.euspace.common.utils.ErrorBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;

class StatelessAuthenticationFilter extends GenericFilterBean {

	private final TokenAuthenticationService tokenAuthenticationService;

	private static final Logger LOGGER = LoggerFactory.getLogger(StatelessAuthenticationFilter.class);

	private ErrorBuilder errorBuilder;

	protected StatelessAuthenticationFilter(TokenAuthenticationService taService, ErrorBuilder errorBuilder) {
		this.tokenAuthenticationService = taService;
		this.errorBuilder = errorBuilder;
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;

		Authentication auth = tokenAuthenticationService.getAuthentication((HttpServletRequest) req);
		if (auth == null) {
			LOGGER.info("auth is null for {}", request.getServletPath());
			HttpServletResponse response = (HttpServletResponse) res;
			ObjectMapper mapper = new ObjectMapper();
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			response.setContentType("application/json");
			CustomErrorType sessionError = new CustomErrorType(errorBuilder.buildGeneral(
					"error.InvalidSessionKey",
					request.getLocale()));
			response.getOutputStream().print(mapper.writeValueAsString(sessionError));

			return;
		}
		SecurityContextHolder.getContext().setAuthentication(auth);
		chain.doFilter(req, res); // always continue
	}

}
