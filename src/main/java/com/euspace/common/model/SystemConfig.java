package com.euspace.common.model;

import java.io.Serializable;

public class SystemConfig implements Serializable {

	private static final long serialVersionUID = -1127014670532376336L;

	private String configKey;
	private String configValue;
	private String environment;

	public SystemConfig() {
		//Unused
	}

	public String getConfigKey() {
		return configKey;
	}

	public void setConfigKey(String configKey) {
		this.configKey = configKey;
	}

	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

}