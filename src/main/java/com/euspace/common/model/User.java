package com.euspace.common.model;

import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.userdetails.UserDetails;

import com.euspace.common.security.UserAuthority;
import com.euspace.common.security.UserRole;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class User extends BaseModel implements UserDetails {

	private static final long serialVersionUID = 7521814421179718618L;
	private static final String ROLE_PREFIX = "ROLE_";

	private String userId;

	@JsonIgnore
	private String username;
	
	private String email;
	private String firstName;
	private String lastName;

	private String password;



	private long dateOfBirth;

	private String profileImage;

	private String phoneNumber;

	@JsonIgnore
	private long expires;

	@JsonIgnore
	private boolean accountExpired;

	@JsonIgnore
	private boolean accountLocked;

	@JsonIgnore
	private boolean credentialsExpired;

	@JsonIgnore
	private boolean accountEnabled;

	private Set<UserAuthority> authorities;

	public User() {
		super();
	}

	// For session purpose
	@JsonIgnore
	private String sessionKey;

	@JsonIgnore
	public String getSessionKey() {
		return sessionKey;
	}

	@JsonIgnore
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public void setAuthorities(Set<UserAuthority> authorities) {
		this.authorities = authorities;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setNumber(String contactNumber) {
		this.phoneNumber = contactNumber;
	}

	public long getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(long dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean isAccountExpired() {
		return accountExpired;
	}

	public void setAccountExpired(boolean accountExpired) {
		this.accountExpired = accountExpired;
	}

	public boolean isAccountLocked() {
		return accountLocked;
	}

	public void setAccountLocked(boolean accountLocked) {
		this.accountLocked = accountLocked;
	}

	public boolean isCredentialsExpired() {
		return credentialsExpired;
	}

	public void setCredentialsExpired(boolean credentialsExpired) {
		this.credentialsExpired = credentialsExpired;
	}

	public boolean isAccountEnabled() {
		return accountEnabled;
	}

	public void setAccountEnabled(boolean accountEnabled) {
		this.accountEnabled = accountEnabled;
	}

	@Override
	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	@JsonIgnore
	public Set<UserAuthority> getAuthorities() {
		return authorities;
	}

	@JsonIgnore
	public Set<Role> getRoles() {
		return new HashSet<>();
	}

	@JsonIgnore
	public void setRoles(Set<UserRole> roles) {
		for (UserRole role : roles) {
			grantRole(role);
		}
	}

	public void grantRole(UserRole role) {
		if (authorities == null) {
			authorities = new HashSet<>();
		}
		final UserAuthority authority = new UserAuthority();
		authority.setAuthority(ROLE_PREFIX + role.toString());
		authority.setUser(this);
		authorities.add(authority);
	}

	public void revokeRole(Role role) {
		if (authorities != null) {
			final UserAuthority authority = new UserAuthority();
			authority.setAuthority(ROLE_PREFIX + role.getRoleCode());
			authority.setUser(this);
			authorities.remove(authority);
		}
	}

	public boolean hasRole(Role role) {
		final UserAuthority authority = new UserAuthority();
		authority.setAuthority(ROLE_PREFIX + role.getRoleCode());
		authority.setUser(this);
		return authorities.contains(authority);
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonExpired() {
		return !accountExpired;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonLocked() {
		return !accountLocked;
	}

	@Override
	@JsonIgnore
	public boolean isCredentialsNonExpired() {
		return !credentialsExpired;
	}

	@Override
	@JsonIgnore
	public boolean isEnabled() {
		return accountEnabled;
	}

	public long getExpires() {
		return expires;
	}

	public void setExpires(long expires) {
		this.expires = expires;
	}

	@Override
	public String toString() {

		String result = "username : " + getUsername() + "\n";
		result += "email : " + getEmail() + "\n";
		result += "expire : " + getExpires() + "\n";
		result += "isAccountExpired : " + isAccountExpired() + "\n";
		result += "isAccountLocked : " + isAccountLocked() + "\n";
		result += "isCredentialsExpired : " + isCredentialsExpired() + "\n";
		result += "isEnabled : " + isAccountEnabled() + "\n";
		return getClass().getSimpleName() + ": " + result;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}