package com.euspace.common.model;

public class RoleAccess extends BaseModel {

	private String roleAccessId;
	private String roleId;
	private String accessName;
	private String accessGroup;
	public String getRoleAccessId() {
		return roleAccessId;
	}

	public void setRoleAccessId(String roleAccessId) {
		this.roleAccessId = roleAccessId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getAccessName() {
		return accessName;
	}

	public void setAccessName(String accessName) {
		this.accessName = accessName;
	}

	public String getAccessGroup() {
		return accessGroup;
	}

	public void setAccessGroup(String accessGroup) {
		this.accessGroup = accessGroup;
	}

}