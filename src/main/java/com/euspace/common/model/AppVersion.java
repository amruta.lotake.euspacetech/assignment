package com.euspace.common.model;

import java.io.Serializable;
import java.util.List;

public class AppVersion extends BaseModel implements Serializable{

	private static final long serialVersionUID = 7230857989548987211L;

	private String appVersionId;

	private String versionNo;

	private String appType;

	private String deviceType;

	private String appLink;

	private long releaseDate;

	private boolean isMandatory;

	private long removeSupportFrom;

	private List<AppVersionReleaseNote> releaseNotes;

	public String getAppVersionId() {
		return appVersionId;
	}

	public void setAppVersionId(String appVersionId) {
		this.appVersionId = appVersionId;
	}

	public String getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}

	public String getAppType() {
		return appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getAppLink() {
		return appLink;
	}

	public void setAppLink(String appLink) {
		this.appLink = appLink;
	}

	public long getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(long releaseDate) {
		this.releaseDate = releaseDate;
	}

	public boolean isMandatory() {
		return isMandatory;
	}

	public void setMandatory(boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	public long getRemoveSupportFrom() {
		return removeSupportFrom;
	}

	public void setRemoveSupportFrom(long removeSupportFrom) {
		this.removeSupportFrom = removeSupportFrom;
	}

	public List<AppVersionReleaseNote> getReleaseNotes() {
		return releaseNotes;
	}

	public void setReleaseNotes(List<AppVersionReleaseNote> releaseNotes) {
		this.releaseNotes = releaseNotes;
	}

}
