package com.euspace.common.model;

import java.io.Serializable;

public class AppVersionReleaseNote extends BaseModel implements Serializable {

	private static final long serialVersionUID = 508035218201231549L;

	private String appVersionReleaseNoteId;

	private String appVersionId;

	private String languageCode;

	private String releaseNotes;

	public String getAppVersionReleaseNoteId() {
		return appVersionReleaseNoteId;
	}

	public void setAppVersionReleaseNoteId(String appVersionReleaseNoteId) {
		this.appVersionReleaseNoteId = appVersionReleaseNoteId;
	}

	public String getAppVersionId() {
		return appVersionId;
	}

	public void setAppVersionId(String appVersionId) {
		this.appVersionId = appVersionId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getReleaseNotes() {
		return releaseNotes;
	}

	public void setReleaseNotes(String releaseNotes) {
		this.releaseNotes = releaseNotes;
	}

}
