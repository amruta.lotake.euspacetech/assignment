package com.euspace.common.model;

import java.util.List;
import java.util.Map;

import com.euspace.common.utils.MessageItem;

public class CustomErrorType {

	private Map<String, List<MessageItem>> errors;

	public CustomErrorType() {

	}

	public CustomErrorType(Map<String, List<MessageItem>> errors) {
		this.errors = errors;
	}

	public Map<String, List<MessageItem>> getErrors() {
		return errors;
	}

	public void setErrors(Map<String, List<MessageItem>> errors) {
		this.errors = errors;
	}

}
