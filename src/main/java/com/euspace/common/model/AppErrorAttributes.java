package com.euspace.common.model;

import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.web.context.request.WebRequest;

import com.euspace.common.utils.Constants;
import com.euspace.common.utils.ErrorBuilder;

public class AppErrorAttributes extends DefaultErrorAttributes {

	private static final Logger LOGGER = LoggerFactory.getLogger(AppErrorAttributes.class);

	private ErrorBuilder builder;

	public AppErrorAttributes() {
		// unused
	}

	public AppErrorAttributes(ErrorBuilder builder) {
		this.builder = builder;
	}

	@Override
	public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {
		Map<String, Object> errorAttributes = super.getErrorAttributes(webRequest, includeStackTrace);
		Locale locale = webRequest.getLocale();
		LOGGER.info(locale.getLanguage());
		String exceptionClass = (String) errorAttributes.get("exception");
		LOGGER.info("Error key : {}", exceptionClass);
		if (Constants.KNOWN_EXCEPTIONS.contains(exceptionClass)) {
			return builder.errorAttributes(builder.buildGeneral(exceptionClass, locale));
		}
		return builder.errorAttributes(builder.buildGeneral("error.UnexpectedError", locale));
	}

}
