package com.euspace.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Role {
	private int roleId;
	private String roleName;
	@JsonIgnore
	private String roleCode;

	public Role() {

	}

	public Role(int roleId, String roleName, String roleCode) {
		super();
		this.roleId = roleId;
		this.roleName = roleName;
		this.roleCode = roleCode;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
}