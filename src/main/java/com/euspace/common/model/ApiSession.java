package com.euspace.common.model;

import com.euspace.common.utils.UUID;

public class ApiSession extends BaseModel {

	private String apiSessionId;
	private String userId;
	private String deviceUUID;
	private long expiresAt;

	public ApiSession() {

	}

	public ApiSession(String userId) {
		this.userId = userId;
		this.apiSessionId = UUID.getId();
	}

	public String getApiSessionId() {
		return apiSessionId;
	}

	public void setApiSessionId(String apiSessionId) {
		this.apiSessionId = apiSessionId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDeviceUUID() {
		return deviceUUID;
	}

	public void setDeviceUUID(String deviceUUID) {
		this.deviceUUID = deviceUUID;
	}

	public long getExpiresAt() {
		return expiresAt;
	}

	public void setExpiresAt(long expiresAt) {
		this.expiresAt = expiresAt;
	}

}