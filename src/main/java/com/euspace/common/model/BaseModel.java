package com.euspace.common.model;

import java.util.UUID;

import com.euspace.common.utils.DateUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class BaseModel {
	@JsonIgnore
	protected boolean active;
	@JsonIgnore
	protected String createdBy;
	@JsonIgnore
	protected String updatedBy;
	@JsonIgnore
	protected long createdAt;
	@JsonIgnore
	protected long updatedAt;
	@JsonIgnore
	protected boolean deleted;
	

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}

	public long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public void updateCCUU(String userId) {
		this.updateCCUU(userId, DateUtils.now());
	}

	public void updateUU(String userId) {
		this.updateUU(userId, DateUtils.now());
	}
	
	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public void updateCCUU(String userId, long now) {
		this.createdAt = now;
		this.updatedAt = now;
		this.createdBy = userId;
		this.updatedBy = userId;
	}

	public void updateUU(String userId, long now) {
		this.updatedAt = now;
		this.updatedBy = userId;
	}
	
	
	public void setCCUUDA(String userId, long timeStamp, boolean deleted, boolean active) {
		createdBy = userId;
		updatedBy = userId;
		createdAt = timeStamp;
		updatedAt = timeStamp;
		this.deleted = deleted;
		this.active = active;
	}

	public static String generateUUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString().replace("-", "");
	}
}
