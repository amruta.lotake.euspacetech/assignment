package com.euspace.common.dto;

import com.euspace.common.utils.Constants;
import com.euspace.common.validator.EmailShouldExist;

public class ForgotPasswordDto {

	@javax.validation.constraints.NotEmpty
	@javax.validation.constraints.Email(regexp = Constants.EMAIL_PATTERN)
	@EmailShouldExist(message = "{EmailIdDoesNotExists}")
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}