package com.euspace.common.dto;

import com.euspace.common.model.AppVersion;

public class AppVersionResponse {

	private String resultCode;
	private String resultMessage;
	private AppVersion latestVersion;

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	public AppVersion getLatestVersion() {
		return latestVersion;
	}

	public void setLatestVersion(AppVersion latestVersion) {
		this.latestVersion = latestVersion;
	}

}
