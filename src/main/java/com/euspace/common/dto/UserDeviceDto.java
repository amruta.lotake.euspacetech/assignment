package com.euspace.common.dto;

import javax.validation.constraints.NotEmpty;

public class UserDeviceDto {

	@NotEmpty
	private String deviceUid;
	@NotEmpty
	private String deviceToken;
	@NotEmpty
	private String deviceName;
	@NotEmpty
	private String deviceType;

	public String getDeviceUid() {
		return deviceUid;
	}

	public void setDeviceUid(String deviceUid) {
		this.deviceUid = deviceUid;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

}