package com.euspace.common.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class UserAddDto {

	private String userId;

	@NotEmpty()
	@Email
	private String email;

	@NotEmpty
	@Size(min = 1, max = 100, message = "{nameSizeError}")
	private String firstName;
	@NotEmpty
	private String lastName;

	@NotEmpty
	private String password;

	private String dateOfBirth;

	private String profileImage;

	private String phoneNumber;

	private UserDeviceDto deviceInfo;

	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public UserDeviceDto getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(UserDeviceDto deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}