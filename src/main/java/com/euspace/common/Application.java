package com.euspace.common;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.Filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Profile;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import com.euspace.common.model.AppErrorAttributes;
import com.euspace.common.security.CustomCorsFilter;
import com.euspace.common.security.CustomeMvcConfig;
import com.euspace.common.security.MIPermissionEvaluator;
import com.euspace.common.security.PublicUrlConfigurer;
import com.euspace.common.utils.ErrorBuilder;
import com.euspace.common.utils.MessageUtils;

@Configuration
@ComponentScan(basePackages = { "com.euspace" })
@EnableAutoConfiguration
@EnableAspectJAutoProxy
@EnableWebMvc
@SpringBootApplication
public class Application {
	public static void main(String[] args) {
		
		ApplicationContext ctx = SpringApplication.run(Application.class, args);
		DispatcherServlet dispatcherServlet = (DispatcherServlet) ctx.getBean("dispatcherServlet");
		
		dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);

	}

	@Bean
	public Filter characterEncodingFilter() {
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");
		characterEncodingFilter.setForceEncoding(true);
		return characterEncodingFilter;
	}

	@Bean
	public ErrorAttributes errorAttributes(ErrorBuilder builder) {
		return new AppErrorAttributes(builder);
	}

	@Bean // Magic entry
	public DispatcherServlet dispatcherServlet() {
		DispatcherServlet ds = new DispatcherServlet();
		ds.setDetectAllHandlerAdapters(true);
		ds.setDetectAllHandlerExceptionResolvers(true);
		ds.setDetectAllHandlerMappings(true);
		ds.setDetectAllViewResolvers(true);
		ds.setThrowExceptionIfNoHandlerFound(true);
		return ds;
	}

	@Bean
	public CustomCorsFilter customCorsFilter() {
		return new CustomCorsFilter();
	}

	@Bean
	public PermissionEvaluator permission() {
		return new MIPermissionEvaluator();
	}


	@Bean
	public ErrorBuilder errorBuilder() {
		return new ErrorBuilder(messageUtils());
	}

	@Bean
	public MessageUtils messageUtils() {
		return new MessageUtils();
	}

	@Bean
	public LocaleResolver localeResolver() {
		return new AcceptHeaderLocaleResolver();
	}

	@Bean
	public PublicUrlConfigurer publicUrlConfigurer() {
		return new PublicUrlConfigurer(Stream.of(
		//@formatter:off
				new String[] { "post", "/api/login" }, 
				new String[] { "get", "/" }, 
				new String[] { "get", "/error" }, 
			    new String[] { "post", "/api/signup" }
//				new String[] { "Get", "/api/user-details"},
			
		//@formatter:on
		).collect(Collectors.toList()));
	}
 
	@Bean
	public CustomeMvcConfig customeMvcConfig() {
		return new CustomeMvcConfig();
	}

	@Bean
	@Profile({ "dev", "demo", "local" })
	public CommonsRequestLoggingFilter logFilter() {
		CommonsRequestLoggingFilter filter = new CommonsRequestLoggingFilter();
		filter.setIncludeQueryString(true);
		filter.setIncludePayload(true);
		filter.setMaxPayloadLength(10000);
		filter.setIncludeHeaders(true);
		filter.setAfterMessagePrefix("REQUEST DATA : ");
		return filter;
	}

}
