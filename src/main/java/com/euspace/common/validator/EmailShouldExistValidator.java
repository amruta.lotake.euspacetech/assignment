package com.euspace.common.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.euspace.common.service.UserService;
import com.euspace.common.service.impl.UserServiceImpl;
import com.euspace.common.utils.EmailUtils;

public class EmailShouldExistValidator implements ConstraintValidator<EmailShouldExist, String> {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmailShouldExistValidator.class);
	
	private UserService userService;

	@Autowired
	public void setUserService(UserServiceImpl userService) {
		this.userService = userService;
	}

	@Override
	public void initialize(EmailShouldExist paramA) {
		// Not required.
	}

	@Override
	public boolean isValid(String email, ConstraintValidatorContext ctx) {
		LOGGER.info("email : " + email);
		if (email == null) {
			return true;
		}
		if (!EmailUtils.isValidEmail(email)) {
			return true;
		}

		return userService.isUserEmailExist(email);
	}

}