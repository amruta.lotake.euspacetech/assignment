package com.euspace.common.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LatitudeValidator implements ConstraintValidator<Latitude, Double> {

	private static final String LATITUDE_REGEX = "^(\\+|-)?((\\d((\\.)|\\.\\d{1,})?)|(0*?[0-8]\\d((\\.)|\\.\\d{1,})?)|(0*?90((\\.)|\\.0{1,})?))$";

	@Override
	public void initialize(Latitude paramA) {
		// Unused
	}

	@Override
	public boolean isValid(Double latitude, ConstraintValidatorContext ctx) {
		return validate(latitude);

	}

	public static boolean validate(Double latitude) {

		String latitudeStr = String.valueOf(latitude);

		return (latitudeStr.matches(LATITUDE_REGEX));
	}

}