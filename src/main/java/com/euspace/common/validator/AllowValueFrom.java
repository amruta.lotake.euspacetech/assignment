package com.euspace.common.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;


@Documented
@Constraint(validatedBy = AllowValueFromValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface AllowValueFrom {


	String message() default "{ValueFrom}";
	String[] allowedValues() default {};

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}