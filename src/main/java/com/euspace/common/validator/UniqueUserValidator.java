package com.euspace.common.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.euspace.common.service.UserService;
import com.euspace.common.service.impl.UserServiceImpl;

public class UniqueUserValidator implements ConstraintValidator<UniqueUser, String> {

	private final Logger logger = LoggerFactory.getLogger(UniqueUserValidator.class);
	
	private UserService userService;

	@Autowired
	public void setUserService(UserServiceImpl userService) {
		this.userService = userService;
	}

	@Override
	public void initialize(UniqueUser paramA) {
		//Unused
	}

	@Override
	public boolean isValid(String username, ConstraintValidatorContext ctx) {
		logger.info("username : " + username);
		if (username == null) {
			return true;
		}
		return (!userService.isUserExist(username));
	}

}