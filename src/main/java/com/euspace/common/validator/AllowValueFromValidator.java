package com.euspace.common.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AllowValueFromValidator implements ConstraintValidator<AllowValueFrom, String> {

	private String[] allowedValues;

	@Override
	public void initialize(AllowValueFrom paramA) {
		// Unused
		this.allowedValues = paramA.allowedValues();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext ctx) {
		if (value == null) {
			return true;
		}
		for (String allowedValue : allowedValues) {
			if (allowedValue.equalsIgnoreCase(value)) {
				return true;
			}
		}
		return false;

	}

}