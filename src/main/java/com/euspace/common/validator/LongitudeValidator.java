package com.euspace.common.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LongitudeValidator implements ConstraintValidator<Longitude, Double> {

	private static final String LONGITUDE_REGEX = "^(\\+|-)?((\\d((\\.)|\\.\\d{1,})?)|(0*?\\d\\d((\\.)|\\.\\d{1,})?)|(0*?1[0-7]\\d((\\.)|\\.\\d{1,})?)|(0*?180((\\.)|\\.0{1,})?))$";

	@Override
	public void initialize(Longitude paramA) {
		// Unused
	}

	@Override
	public boolean isValid(Double longitude, ConstraintValidatorContext ctx) {
		return validate(longitude);

	}

	public static boolean validate(Double longitude) {

		String longitudeStr = String.valueOf(longitude);

		return (longitudeStr.matches(LONGITUDE_REGEX));
	}

}