package com.euspace.common.controller.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.euspace.common.controller.BaseRestController;
import com.euspace.common.dto.UserAddDto;
import com.euspace.common.exception.DBException;
import com.euspace.common.model.Role;
import com.euspace.common.service.UserService;
import com.euspace.common.utils.Constants;
import com.euspace.common.utils.DateUtils;
import com.euspace.common.utils.StringUtils;

@RestController
public class SignUpController extends BaseRestController {
	public static final String SIGNIN_FILED_REQUIRED = "users.signInFieldRequired";
	private static final String INVALID_LOGIN_CREDENTIALS = "users.invalidLoginCredentials";
	private static final Logger LOGGER = LoggerFactory.getLogger(SignUpController.class);

	@Autowired
	UserService userService;

	@RequestMapping(value = "/api/signup", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> addUser(HttpServletRequest request, @RequestBody @Valid final UserAddDto user) {

		user.setEmail(user.getEmail().toLowerCase().trim());

		if (user.getPhoneNumber() == "" || !StringUtils.isPhoneNumberValid(user.getPhoneNumber())) {
			return constructFailureResponse("user.phoneNumberError", HttpStatus.NOT_FOUND, request.getLocale());
		}

		boolean phoneNumberStatus = userService.isPhoneNumberExistsForOtherUsers(user.getPhoneNumber(),
				Constants.EMPTY_STRING);

		if (phoneNumberStatus) {
			return constructFailureResponse("user.phoneNumberAlreadyExists", HttpStatus.NOT_FOUND, request.getLocale());
		}

		if (StringUtils.isEmpty(user.getEmail())) {
			return constructFailureResponse("user.emailError", HttpStatus.NOT_FOUND, request.getLocale());
		}

		user.setEmail(user.getEmail().toLowerCase().trim());

		boolean emailStatus = userService.isEmailExistsForOtherUsers(user.getEmail(), Constants.EMPTY_STRING);

		if (emailStatus) {
			return constructFailureResponse("user.emailAlreadyExists", HttpStatus.NOT_FOUND, request.getLocale());
		}

		List<Role> roles = new ArrayList<>();
		roles.add(new Role(Constants.ROLE_USER_ID, Constants.ROLE_USER_STR, Constants.ROLE_CODE_USER_STR));
		Map<String, Object> result = userService.signupUser(user, roles);
		if (result != null) {

			return constructSuccessResponseForObject(HttpStatus.OK, request.getLocale(), result);
		}
		return constructFailureResponse("error.UnexpectedError", HttpStatus.NOT_FOUND, request.getLocale());
	}

	@RequestMapping(value = "/api/login", method = RequestMethod.POST)
	public ResponseEntity<?> signIn(HttpServletRequest request, @RequestBody UserAddDto user) {

		String userId = "";
		long now = DateUtils.now();
		String field;
		String value;

		if (!StringUtils.isEmpty(user.getEmail()) && !StringUtils.isEmpty(user.getPassword())) {

			field = "email";
			value = user.getEmail();

		} else {

			return constructFailureResponse(SIGNIN_FILED_REQUIRED, HttpStatus.BAD_REQUEST, request.getLocale());

		}

		Map<String, Object> validatedUser = userService.validateUserSignIn(field, value, user);

		if (!validatedUser.isEmpty()) {
			return constructSuccessResponseForObject(HttpStatus.OK, request.getLocale(), validatedUser);
		}

		return constructFailureResponse(INVALID_LOGIN_CREDENTIALS, HttpStatus.BAD_REQUEST, request.getLocale());

	}
}