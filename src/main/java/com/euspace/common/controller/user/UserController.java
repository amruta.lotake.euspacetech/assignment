package com.euspace.common.controller.user;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.BadRequestException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session.Cookie;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.euspace.common.controller.BaseRestController;
import com.euspace.common.dto.UserAddDto;
import com.euspace.common.dto.UserDto;
import com.euspace.common.model.User;
import com.euspace.common.service.UserService;
import com.euspace.common.utils.DateUtils;
import com.euspace.common.utils.StringUtils;

@RestController
public class UserController extends BaseRestController {
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	private static final String USER_ID_REQUIRED_ERROR = "user.userIdRequiredError";

	@Autowired
	UserService userService;
	
	@RequestMapping(value = "/api/user-details/{tabId}", method = RequestMethod.GET)
	public ResponseEntity<?> getUserDetailsById(HttpServletRequest request,@PathVariable String  tabId) {
		int tabid=Integer.parseInt(tabId);  
		if(tabid > 1) {
			return constructFailureResponse("user.userAlreadyLoggedIn", HttpStatus.NOT_FOUND, request.getLocale());	
		}
		
		Optional<User> userFromSession = loggedInUser();

		if (!userFromSession.isPresent()) {

			return constructFailureResponse("failure.failToAccessRecord", HttpStatus.NOT_FOUND, request.getLocale());
		}
		
		String userId = userFromSession.get().getUserId();
		Optional<UserDto> userInformationDetailsDto = userService.getUserCompleteDetailsByUserId(userId);

		if (userInformationDetailsDto.isPresent()) {

			return constructSuccessResponseForObject(HttpStatus.OK, request.getLocale(), userInformationDetailsDto.get());

		} else {
			return constructFailureResponse("user.detailsNotFound", HttpStatus.NOT_FOUND, request.getLocale());

		}
	}
	
	
}