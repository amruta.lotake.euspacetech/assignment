package com.euspace.common.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.euspace.common.utils.UUID;

@Controller
public class ROOTController {

	private static final String CONST_YES = "yes";

	@RequestMapping(value = "/", method = RequestMethod.GET, produces = "text/html")
	public ModelAndView index() {
		return new ModelAndView("index");
	}
	
	@RequestMapping(value = "/test-link", method = RequestMethod.GET, produces = "text/html")
	public ModelAndView testLink(@RequestParam(name = "param", required= true) String param) {
		
		if(CONST_YES.equalsIgnoreCase(param)){
			return new ModelAndView("successLink");
		}
		ModelAndView failed =  new ModelAndView("failedLink");
		failed.addObject("message", UUID.getId());
		
		return failed;
	}

}