package com.euspace.common.controller;

import java.util.Locale;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.euspace.common.model.User;
import com.euspace.common.security.UserAuthentication;
import com.euspace.common.utils.MessageItem;
import com.euspace.common.utils.MessageUtils;

public class BaseRestController {
	@Autowired
	protected MessageUtils messages;

	protected <T> ResponseEntity<T> apiResponse(T t) {
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	protected <T> ResponseEntity<T> apiResponse(T t, HttpStatus status) {
		return new ResponseEntity<>(t, status);
	}
	
	protected ResponseEntity<Object> constructSucccessResponse(String messageKey, HttpStatus status, Locale locale) {
		String message = messages.getMessage(messageKey, locale);
		MessageItem messageItem = new MessageItem(messageKey, message);

		return new ResponseEntity<>(messageItem, status);
	}
	
	protected ResponseEntity<Object> constructFailureResponse(String messageKey, HttpStatus status, Locale locale) {
		String message = messages.getMessage(messageKey, locale);
		MessageItem messageItem = new MessageItem(messageKey, message);

		return new ResponseEntity<>(messageItem, status);
	}

	protected ResponseEntity<Object> constructSuccessResponse(String messageKey, HttpStatus status, Locale locale,
			Object... params) {
		String message = messages.getMessage(messageKey, locale, params);
		MessageItem messageItem = new MessageItem(messageKey, message);

		return new ResponseEntity<>(messageItem, status);
	}
	
	protected ResponseEntity<Object> constructSuccessResponseForObject(HttpStatus status, Locale locale,
			Object params) {
		

		return new ResponseEntity<>(params, status);
	}

	protected Optional<User> loggedInUser() {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserAuthentication) authentication).getDetails();
		return Optional.ofNullable(user);
	}
}
