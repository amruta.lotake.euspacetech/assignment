package com.euspace.common.controller.appversion;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.euspace.common.controller.BaseRestController;
import com.euspace.common.dto.AppVersionResponse;
import com.euspace.common.exception.AppVersionException;
import com.euspace.common.model.AppVersion;
import com.euspace.common.service.AppVersionService;
import com.euspace.common.utils.Constants;

@RestController
@RequestMapping("/api")
public class VersionController extends BaseRestController {

	@Autowired
	private AppVersionService versionService;

	@RequestMapping(value = "/version/{appType}/{deviceType}/{version:.+}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public AppVersionResponse retriveVersion(@PathVariable("appType") String appType, @PathVariable("deviceType") String deviceType, @PathVariable("version") String version, WebRequest request) {

		if (!Constants.DeviceTypes.SUPPORTED.contains(deviceType.toLowerCase())) {
			throw new AppVersionException("invalid.device.type");
		}

		if (!Constants.AppTypes.SUPPORTED.contains(appType.toLowerCase())) {
			throw new AppVersionException("invalid.app.type", Constants.AppTypes.CUSTOMER);
		}

		long currentDate = new Date().getTime();
		AppVersion currentVersion = versionService.retriveVersionByVersion(appType, deviceType, version);
		if (currentVersion == null) {
			throw new AppVersionException("error.version");
		}
		AppVersion latestVersion = versionService.retriveLatestVersion(appType, deviceType);
		latestVersion.setReleaseNotes(versionService.retriveReleaseNotesForVersion(latestVersion.getAppVersionId()));
		AppVersionResponse response = new AppVersionResponse();
		response.setLatestVersion(latestVersion);
		if (latestVersion.getVersionNo().equalsIgnoreCase(currentVersion.getVersionNo())) {
			response.setResultCode(Constants.AppVersion.NO_NEW_RELEASE);
			response.setResultMessage("msg.release.no.update");
		} else if (currentVersion.getRemoveSupportFrom() <= currentDate) {
			response.setResultCode(Constants.AppVersion.UNSUPPORTED_RELEASE);
			response.setResultMessage("msg.release.support.removed");
		} else if (latestVersion.isMandatory() || versionService.checkMandatoryReleaseAvailable(appType, deviceType, currentVersion.getReleaseDate())) {
			response.setResultCode(Constants.AppVersion.MANDATORY_RELEASE);
			response.setResultMessage("msg.release.mandatory");
		} else {
			response.setResultCode(Constants.AppVersion.OPTIONAL_RELEASE);
			response.setResultMessage("msg.release.optional");
		}

		return response;
	}

}
